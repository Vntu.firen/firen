<?php
	$title = "КАФЕДРИ";
?>


                        <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>


У структуру <b>ФРТТК</b> входять три кафедри: радіотехніки (завідувач - д. т. н., проф. О. В. Осадчук), яка є випускною для бакалаврського напряму та спеціальності інженерії <радіотехніка>; <br><br>
<a class=neu href='http://rt.vntu.edu.ua/' target=_blank><b>Сайт кафедри радіотехніки</b></a><br><br>

телекомунікаційних систем і телебачення (завідувач - д. т. н., проф. В. М. Кичак), яка є випускною для бакалаврського напряму <телекомунікації> та спеціальності інженерії <апаратура радіозв'язку, радіомовлення і телебачення><br><br>
<a class=neu href='../tkstb' target=_blank><b>Сайт кафедри телекомунікаційних систем і телебачення</b></a><br><br>

 та загальноуніверситетська кафедра мовознавства (завідувач -Л.Є.Азарова).<br><br>
<a class=neu href='http://kmz.vntu.edu.ua/' target=_blank><b>Сайт кафедри мовознавства</b></a><br><br>

Високий рівень підготовки на ФРТТК підтримується завдяки науковій діяльності співробітників і студентів. На факультеті функціонує науково-дослідна лабораторія, в якій під керівництвом доктора технічних наук, професора В. М. Кичака на замовлення різних підприємств і фірм виконується ряд науково-дослідних і дослідно-конструкторських робіт телекомунікаційного спрямування.<br>






<br><br>
<b><Радіотехніка></b><br>
<i>Випускники цієї спеціальності займаються розробкою та експлуатацією сучасних радіотехнічних пристроїв і систем, призначених для формування, передавання та приймання сигналів, їх аналогового та цифрового оброблення, а також пристроїв радіоавтоматики, радіокерування та радіоелектронної апаратури на базі найновіших досягнень обчислювальної техніки та мікропроцесорів, мікроелектроніки та функціональної електроніки.</i><br><br>

<b><Апаратура радіозв'язку, радіомовлення і телебачення></b><br>
<i>Отримана кваліфікація дозволяє проектувати та експлуатувати сучасні пристрої, що функціонують в різних діапазонах хвиль і призначені для передавання, приймання та оброблення в аналогових і цифрових комплексах різноманітних видів інформації: зображення, мови, сигналів керування та телеметрії, цифрових даних. Фахівці цієї спеціальності володіють сучасними технологіями радіозв'язку, знаннями в галузі стільникового та супутникового зв'язку, технологіями використання радіочастотного ресурсу.</i><br><br>

<b><Телекомунікаційні системи та мережі></b><br>
<i>Фахівці цієї спеціальності отримують ґрунтовну підготовку у виробничій, організаційно-управлінській, проектній та науковій діяльності в галузі розробки та експлуатації телекомунікаційних систем і мереж на підприємствах зв'язку, в наукових, конструкторських і проектних організаціях та фірмах. Підготовка спеціалістів-телекомунікаційників здійснюється відповідно Національної програми розвитку мереж зв'язку України з підвищення ефективності функціонування мереж електрозв'язку загального та спеціального призначення, покращення якості наданих послуг і автоматизації технічних способів зв'язку.</i><br><br>

<br>
До складу  <b>ФМБЕП</b> входять кафедри: проектування медико-біологічної апаратури (ПМБА), 
<br><br>
<a class=neu href='../pmba' target=_blank><b>Сайт кафедри ПМБА</b></a><br><br>

проектування комп'ютерної та телекомунікаційної апаратури (ПКТА) 

<br><br>
<a class=neu href='../pkta' target=_blank><b>Сайт кафедри ПКТА</b></a><br><br>

та кафедра інтеграції навчання з виробництвом (ІНВ), 
<br><br>
<a class=neu href='http://ininv.vntu.edu.ua/ukr/' target=_blank><b>Сайт кафедри ІНВ</b></a><br><br>

кафедра електроніки
<br><br>
<a class=neu href='../ke' target=_blank><b>Сайт кафедри електроніки</b></a><br><br>

які мають навчальні та наукові лабораторії, що оснащені сучасним обладнанням.<br>
<br>
<b><Виробництво електронних засобів></b><br>
<i>Студенти цієї спеціальності конструюють комп'ютерну техніку різного призначення і проектують технології її виготовлення; розробляють і професійно експлуатують програмне забезпечення інформаційно-вимірювальних мікропроцесорних систем і систем керування; опрацьовують схемотехнічні рішення систем оброблення інформації, складних промислових і побутових приладів; ведуть теоретичні, системні та прикладні дослідження в галузі виробництва електронних засобів, використання нових фізичних ефектів і явищ; професійно застосовують комп'ютерну техніку у власній інженерній діяльності, на виробництві, в офісі.</i><br><br>

<b><Біотехнічні та медичні апарати і системи></b><br>
<i>Фахівці цієї спеціальності обслуговують медичну апаратуру різного рівня складності, що використовується в медичних лікувальних, профілактичних, оздоровчих та наукових установах, а також займаються її розробкою у співпраці з медичними фахівцям, проводять наукові дослідження.</i><br><br>

<b><Технології та засоби телекомунікацій></b><br>
<i>Інженери радіоелектронного апаратобудування цієї спеціальності працюють в державних та комерційних установах, займаються розробкою, експлуатацією та ремонтом цифрових систем передачі, комп'ютерних мереж, мобільних систем зв'язку, систем телеметрії та дистанційного керування, систем кабельного та супутникового телебачення.</i><br><br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp;Випускники ІнРТЗП мають необмежені можливості працевлаштування і можуть трудитись у будь-якій організації, де використовують радіоелектронну та телекомунікаційну апаратуру. Наприклад, випускники 2000-2004 років працюють за фахом у таких організаціях та підприємствах: ВАТ <Укртелеком>, Вінницькому обласному радіотелевізійному передавальному центрі, Вінницькій обласній телерадіокомпанії <Вінниччина>, НДІ <Гелій>, Службі безпеки України, Податковій службі, Держказначействі України, банках <Аваль> та <Приватбанк>, державному підприємстві <Вінницятеплокомуненерго>, ВАТ <Вінницький дослідний завод>, ВАТ <Маяк>, Південно-західній залізниці, в телекомунікаційних і комп'ютерних фірмах <Київстар>, <Тріак>, <Апекс>, <Інтехсервіс>, <ПаркАудіо>, <Інфракон Мікро>, <Хорс-телеком>, <Лінк>, <Радіо>.<br>




</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
