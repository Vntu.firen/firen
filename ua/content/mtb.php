<?php
	$title = "МАТЕРІАЛЬНО-ТЕХНІЧНА БАЗА";
?>

                        <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>

<b>Базова станція RBS 2202 системи стільникового рухомого зв'язку стандарту GSM 900/1800 з функціями GPRS і EDGE</b>
<br><br>
<img src=img/RBS22_1.jpg height=200>
<img src=img/RBS22_2.jpg height=200>
<img src=img/RBS22_3.jpg height=200>
<br><br>
Виробник: Ericsson AB SE-164 80,Stockholm, Sweden<br><br>
<i>Ericsson (Telefonaktiebolaget LM Ericsson) - шведська компанія, відомий виробник телекомунікаційного обладнання</i>
<br><br>

<b>Cпрямовані антени VPol Panel, діапазони 900/1800/1900 МГц</b>
<br><br>
<img src=img/kathrein_1.jpg height=200>
<img src=img/4G-cross-polarisation-45-135.png  width=300>
<br><br>
Виробник: Компанія KATHREIN WERKE KG ( Німеччина ) <br><br>
<i>KATHREIN WERKE KG є світовим лідером у виробництві базових антен рухомого зв'язку з щомісячним обсягом виробництва більше 20 000 штук на місяць. Більше 140 операторів зв'язку в усьому світі використовують продукцію компанії.

Компанія KATHREIN випускає щонайширший асортимент антенного обладнання з діапазоном частот від 25 до 3800 МГц , включаючи направлення і всеспрямовані антени , виготовлені під індивідуальні вимоги , а також спеціальний клас антен для використання на транспорті. Тільки в діапазоні 790-1900 МГц виробляються більше 250 типів антен.</i>
<br><br>

<b>Цифрова АТС - ipLDK-60 з функціями VoIP, ISDN PRI, функцію Caller ID (CID) і АОН, сервіс коротких повідомлень (SMS)</b>
<br><br>
<img src=img/ipdlk60_1.jpg height=200>
<img src=img/ipdlk60_2.jpg height=200>
<img src=img/ipdlk60_3.jpg height=200>
<br><br>
Виробник: LG-Nortel <br><br>
<i>ipLDK -60 - цифрова АТС , оптимізована для компаній малого і середнього бізнесу, має широкий набір послуг зв'язку , необхідних сучасному підприємству : підтримує VoIP , ISDN PRI , функцію Caller ID ( CID ) і російський АОН , сервіс коротких повідомлень ( SMS). Вбудовані сервіси голосової пошти та автосекретаря , використовуються для привітання клієнтів і створення персональних голосових поштових скриньок. Також АТС пропонує користувачам додаткові функціональні можливості, такі як ТАРI інтерфейс для інтеграції програм PC phone , PC- Attendant ; віддалений системний доступ для настройки станції ; розпізнавання тонів в аналогових з'єднувальних лініях , детальне протоколювання з'єднань ( SMDR ) та видачі інформації з аналізу трафіку.</i>
<br><br>

<b>Сиcтеми SIP телефонії 3CX Phone System</b>
<br><br>
<img src=img/3cx_1.png width=153>
<img src=img/3cx_2.png width=150>
<img src=img/3cx_3.png width=149>

<br><br>
Виробник: Компанія 3CX <br><br>
<i>3CX Phone System v.12 SP3 включає нові можливості як для кінцевих користувачів, так і для адміністраторів. Наприклад, тепер можна призначати обраних користувачів Менеджерами груп (Group Managers), Адміністраторами транков (VoIP Trunk Admins) або Адміністраторами системи (System Administrators). Інша корисне поліпшення - можливість групового імпорту DID / DDI з CSV файлу. Це дозволяє, наприклад, швидко об'єднати 3CX Phone System і сторонню SIP АТС (Asterisk, Panasonic і т.п.)</i>
<br><br>

<b>Радіорелейні ситеми MINI-LINK E / MINI-LINK E Micro</b>
<br><br>
<img src=img/MINILINK.jpg width=490>

<br><br>
Виробник: Ericsson Microwane Systems AB<br><br>
<i>Кілька терміналів MINI- LINK E можуть бути інтегровані в один
загальний модуль доступу . Це дозволяє зробити надзвичайно
компактними сайти мережі , а також ефективно розподілити між
різними терміналами такі ресурси , як мультиплексори , інтерфейси
службових каналів та системи підтримки .
Маршрутизація трафіку і його переадресація в межах сайту можуть
виконуватися при мінімальній кількості зовнішніх кабелів.
Маршрут трафіку задається за допомогою ПЗ і конфігурується під
час установки станції.
Термінал може бути налаштований як нерезервовані ( 1 +0 ) або
резервований ( 1 +1 ) ; резервування може бути також забезпечено
мережею кільцевого типу.
Кожен термінал забезпечує швидкість трафіку до 17x2 ( 34 +2 ) Мбіт / с.</i>
<br><br>


<b>ADS-B feeder site</b>
<br><br>
<img src=img/iot_firen.jpg width=490>
<br><br>
Виробник: Altera Cyclone / Raspberry Pi<br><br>
<i>
Автоматичне залежне спостереження - радіомовне (ADS–B) - це технологія коопераційного спостереження, в якій ПС визначає своє місцеположення через супутникову систему навігації та поширює його бортовим відповідачем. Цю інформацію можуть отримувати як наземні станції, в тому числі і органи УПР, так і інші ПС, що дає змогу екіпажам бути більш ситуаційно обізнаними та само-ешелонуватись. 
ADS–B - "автоматичне" - тому що, діє без втручання екіпажу. "Залежне" - тому що, залежить від даних навігаційних систем ПС. 
Технологія ADS-B, яка складається з двох різних сервісів,  "ADS-B Out" та "ADS-B In", цілком може замінити, радіолокатор, як основний засіб спостереження за повітряним рухом. Вона також забезпечує метеорологічною інформацією в "дійсному часі".
"ADS-B Out" двічі в секунду через бортовий передавач поширює інформацію про пізнвальний індекс, точне місцеположення, висоту та швидкість ПС, а також інші дані з ботових систем ПС.
"ADS-B In" приймає інформацію з каналів FIS-B (Польотно-інформаційного сервісу ) та TIS-B (Інформацію про інший рух), а також інші дінні ADS-B, такі як пряме спілкування з ПС які перебувають поблизу.
</i>
<br><br>

<b>Cisco Packet Tracer </b>
<br><br>
<img src=img/PT-logo.png width=400><br>
<img src=img/packet-tracer.png width=300> 

<br><br>
Виробник: Cisco Systems, Inc.<br><br>
<i>
Cisco Packet Tracer - це багатофункціональна програма моделювання мереж, яка дозволяє студентам експериментувати з поведінкою мережі і оцінювати можливі сценарії. Будучи невід'ємною частиною комплексної середовища навчання Мережевої академії, Packet Tracer надає функції моделювання, візуалізації, авторської розробки, атестації та співробітництва, а також полегшує викладання і вивчення складних технологічних принципів.<br>
Cisco Systems є світовим лідером в області мережевих технологій. Мережеві рішення Cisco забезпечують роботу об’єднаних мереж великої кількості компаній, університетів і урядових організацій по всьому світі, а також дозволяють створити мережу будь-якої складності, оптимальним чином використовуючи для цього устаткування Cisco.
</i>
<br><br>

<b>Microsoft DreamSpark Premium</b>
<br><br>
<img src=img/dreamspark_v2.jpg width=480> 

<br><br>
Виробник: Microsoft Corporation<br><br>
<i>
Центр інформаційних технологій і захисту інформації здійснює в університеті безкоштовну академічну підписку DreamSpark Premium (стара назва MSDN AA) на програмне забезпечення Microsoft, що дає можливість безкоштовно встановлювати ліцензійне програмне забезпечення корпорації Microsoft на всі комп’ютери університету, що задіяні в навчальному процесі, а також на всі домашні комп’ютери студентів і викладачів університету. З 2012 року з’явилась також можливість безкоштовного доступу до новітніх хмарних служб Office 365 for Education.<br>
Згідно умов підписки DreamSpark Premium, всі програмні продукти Microsoft, які будуть встановлені на комп’ютери під час дії підписки, а також під час навчання студентів в університеті, можна буде використовувати й після завершення підписки, і по закінченню університету.
</i>
<br><br>


</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
