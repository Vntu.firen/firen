function round(number,places) {
	// rounding numbers
	places = (!places ? 2 : places);
	return Math.round(number*Math.pow(10,places))/Math.pow(10,places);
} 

function log10(x) {
	// Logging10
	return(Math.log(x) / Math.log(10));
}

function from_dbm(u_dbm) {
	dbm = parseFloat(u_dbm);
	
	// validate dbm range
	if ((isNaN(dbm)) || (dbm < -110) || (dbm > 100)) 
	{
  	window.alert("диапазон dBm от -110 до 100");
    return("Error");
  }
	
	mw = Math.pow(10,dbm / 10);
	
	return(round(mw,2));
}

function to_dbm(u_mw) {
	w = parseFloat(u_mw);
	
	// validate milliwatts range
	if ((isNaN(w)) || (w < 0) || (w > 20000)) 
	{
        window.alert("диапазон миливатт от 0 до 20,000");

    	return("Error");
    }
	w = log10(w) / log10(10);
	dbm = round((10 * w),2);
	return(dbm);
}
