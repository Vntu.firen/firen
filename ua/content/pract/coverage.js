function round(num,decimal) {
	// rounding numbers
	decimal = (!decimal ? 2 : decimal);
return Math.round(num*Math.pow(10,decimal))/Math.pow(10,decimal);
} 

function calcDowntilt(angle, actualHgt) {
	// set angle to the opposite value then convert it to radians
	angle = angle / 360;
	angle = angle * (2 * Math.PI);
	// get the tangent of the angle
	tangent = Math.tan(angle);
	// divide height by tangent then divide the total by 5280 to get miles
	distance = (actualHgt / tangent) / 1000;
	return distance;
}

function calcCoverage() {
	// declare variables and set the values
	var antHgt;
	var downtilt;
	var beamwidth;
	var innerRad;
	var outerRad;
	var innerRadDist;
	var outerRadDist;

	antHgt = parseFloat(document.coverage.txtHgt.value);
    downtilt = parseFloat(document.coverage.txtDowntilt.value);
	beamwidth = parseFloat(document.coverage.txtBeamwidth.value);
	
	// error checking
	if ((isNaN(antHgt)) || (antHgt <= 0) || (antHgt > 9999)) {
        window.alert("Высота подвеса антенны в метрах должна быть в диапазоне от 0 до 9999 метров");
		document.coverage.txtHgt.value = "";
		document.coverage.txtHgt.focus();
    	return(0);
    }
	
	if ((isNaN(downtilt)) || (downtilt < 0) || (downtilt > 90)) {
        window.alert("Угол наклона антенныв градучах должен быть в диапазоне от 0 до 90");
		document.coverage.txtDowntilt.value = "";
		document.coverage.txtDowntilt.focus();
    	return(0);
    }
	
	if ((isNaN(beamwidth)) || (beamwidth < 0) || (beamwidth > 99)) {
        window.alert("Ширина луча в градусах должна быть в диапазоне от 0 до 99");
		document.coverage.txtBeamwidth.value = "";
		document.coverage.txtBeamwidth.focus();
    	return(0);
    }
	
	// get half the beamwidth and minus form downtilt to get outer radius, add to get inner radius
	innerRad = downtilt + (beamwidth / 2);
	outerRad = downtilt - (beamwidth / 2);
	
	// run through calcDowntilt function to get distance in miles
	innerRadDist = calcDowntilt(innerRad, antHgt);
	outerRadDist = calcDowntilt(outerRad, antHgt);
	if (innerRadDist <= 0) {
		innerRadDist = "Горизонт";
		document.coverage.txtAnsInnerRad.value = innerRadDist;
	} else if (innerRadDist == "Infinity") {
		innerRadDist = "Горизонт";
		document.coverage.txtAnsInnerRad.value = innerRadDist;
	} else {
		document.coverage.txtAnsInnerRad.value = round(innerRadDist,2);
	}
	
	// if negative number result = Horizon else set the angle in
	// the input field and round off to two decimal places
	if (outerRadDist <= 0) {
		outerRadDist = "Горизонт";
		document.coverage.txtAnsOuterRad.value = outerRadDist;
	} else if (outerRadDist == "Infinity") {
		outerRadDist = "Горизонт";
		document.coverage.txtAnsOuterRad.value = outerRadDist;
	} else {
		document.coverage.txtAnsOuterRad.value = round(outerRadDist,2);
	}
	return(1);
}