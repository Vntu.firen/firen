function round(number,X) 
    { 	
    X = (!X ? 2 : X); 	
    return Math.round(number*Math.pow(10,X))/Math.pow(10,X); 
    }
//rounding 

function Log10(x) 
    { 	
    var Number; 	
    Number = Math.log(x) / Math.log(10); 	
    return(Number); 
    }
//Log10  

function cmdSOM_onclick() 
{ 
var FreqMHz;     
var DistMi; 	
var TXpwr; 	
var TXcabloss; 	
var TXantgain; 	
var RXantgain; 	
var RXcabloss; 	
var RXsens; 	
var FSL;         
FreqMHz = parseFloat(document.margin.txtfsll_Freq.value);
DistMi = parseFloat(document.margin.txtfsll_Dist.value);
document.margin.txtFSL.value = round(20 * Log10(FreqMHz) + 20 * Log10(DistMi) + 32.4,1);
TXpwr = parseFloat(document.margin.txtTXpwr.value);
TXcabloss = parseFloat(document.margin.txtTXcabloss.value);
TXantgain = parseFloat(document.margin.txtTXantgain.value);
RXantgain = parseFloat(document.margin.txtRXantgain.value);
RXcabloss = parseFloat(document.margin.txtRXcabloss.value);
RXsens = parseFloat(document.margin.txtRXsens.value);
FSL = parseFloat(document.margin.txtFSL.value);
document.margin.txtSOM.value = round((TXpwr - TXcabloss + TXantgain - FSL + RXantgain - RXcabloss - RXsens),1);
document.margin.txtRXsig.value = round((TXpwr - TXcabloss + TXantgain - FSL + RXantgain - RXcabloss),1);
return(1); 

}
//cmdsom_onclick  
