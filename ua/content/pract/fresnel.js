function round(num,decimal) {
// rounding numbers
decimal = (!decimal ? 2 : decimal);
return Math.round(num*Math.pow(10,decimal))/Math.pow(10,decimal);
} 

function sqroot(X) {
 // square root
  var Number;
  Number = Math.sqrt(X);
  return(Number);
}

function calcFresnel() {
      var distance1;
      var distance2;
      var frequency;
      var freznelzone;
      var freznelsixty;
     distance = parseFloat(document.main.txtDistance.value);
     frequency = parseFloat(document.main.txtFreq.value);
        
// error checking
   if ((isNaN(distance)) || (distance <0) || (distance > 999)){
           window.alert("Расстояние в км должно быть в диапазоне между 0 и 999");
           document.main.txtDistance.value ="";
	   document.main.txtDistance.focus();
          return(0);
    }

	if ((isNaN(frequency)) || (frequency < .001) || (frequency > 10)){
        window.alert("Частота в ГГц должна быть в диапазоне от 0.001 до 10");
        document.main.txtFreq.value = "";
	document.main.txtFreq.focus();
        return(0);
    }
   // get the two values needed the second is just 80% of the first value
  freznelzone = 17.3 * sqroot(distance / (frequency * 4));
  frezneleighty = .8 * freznelzone;
   
document.main.txtAnsFresZone1.value = round(freznelzone,1);
document.main.txtAnsFresZone2.value = round(frezneleighty,1);
return(1);
}

