<?php
	$title = "СПЕЦІАЛЬНОСТІ І СПЕЦІАЛІЗАЦІЇ";
?>

                        <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>

ФІРЕН ВНТУ здійснює підготовку фахівців на денній і заочній формі навчання по такими спеціальностям і спеціалізаціям:<br><br>





<table border=1 class="c11"><tbody><tr class="c3"><td class="c12" colspan="2" rowspan="1"><p class="c7 c2"><span class="c0"></span></p><p class="c7"><span class="c0">Факультет інфокомунікацій, радіоелектроніки та наносистем</span></p><p class="c2 c7"><span class="c0"></span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1"><span class="c0">15 Автоматизація та приладобудування</span></p><p class="c1 c2"><span class="c0"></span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">153 Мікро- та наносистемна техніка</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1"><span class="c0">16 Хімічна та біоінженерія</span></p><p class="c1 c2"><span class="c0"></span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">163 Біомедична інженерія</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1"><span class="c0">17 Електроніка та телекомунікації</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">171 Електроніка</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1 c2"><span class="c0"></span></p><p class="c1"><span class="c0"> </span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">електронні пристрої та  системи</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="7"><p class="c1"><span class="c0">17 Електроніка та телекомунікації</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">172 Телекомунікації та радіотехніка</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">телекомунікації </span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">інформаційно-телекомунікаційні технології</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">телекомунікаційні системи та мережі</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">радіотехніка</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">інформаційно-обчислювальні засоби радіоелектронних систем</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">інтелектуальні технології мікросистемної радіоелектронної техніки</span></p></td></tr></tbody></table>


<!--

<FONT COLOR="#ff3333"><b>0509 Радіотехніка, приладобудування та зв’язок</b></font><br><br>

<TABLE WIDTH=507 BORDER=1 BORDERCOLOR="#00b8ff" CELLPADDING=4 CELLSPACING=3>
									<COL WIDTH=74>
									<COL WIDTH=406>
									<THEAD>
										<TR VALIGN=TOP>
											<TD ROWSPAN=4 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B><SPAN STYLE="font-style: normal"><FONT FACE="Arial, sans-serif"><FONT COLOR="#ff3333">6.050901</FONT></FONT></SPAN></B></FONT></FONT></FONT></P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>Радіотехніка</B></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.56cm; margin-right: 0.01cm; text-indent: -0.32cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050901-01
												&mdash; Радіотехніка</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.56cm; margin-right: 0.01cm; text-indent: -0.32cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050901-02
												&mdash; Апаратура радіозв'язку, радіомовлення і телебачення  </FONT></FONT></FONT>
												</P>
											</TD>
										</TR>
									</THEAD>
									<TBODY>
										<TR VALIGN=TOP>
											<TD ROWSPAN=3 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>6.050902</B></FONT></FONT></FONT></P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>Радіоелектронні апарати</B></FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.62cm; margin-right: 0.01cm; text-indent: -0.29cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050902-01
												&mdash; Радіоелектронні апарати і засоби</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.62cm; margin-right: 0.01cm; text-indent: -0.29cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050902-02
												&mdash; Біотехнічні та медичні апарати і системи</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR VALIGN=TOP>
											<TD ROWSPAN=4 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>6.050903
												</B></FONT></FONT></FONT>
												</P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>Телекомунікації</B></FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.54cm; margin-right: 0.01cm; text-indent: -0.26cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050903-02
												&mdash; Телекомунікаційні системи та мережі</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.54cm; margin-right: 0.01cm; text-indent: -0.26cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050903-03
												&mdash; Технології та засоби телекомунікацій</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.54cm; margin-right: 0.01cm; text-indent: -0.26cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050903-04
												&mdash; Оргтехніка та звя'зок</FONT></FONT></FONT></P>
											</TD>
										</TR>
									</TBODY>
								</TABLE>


<br><br><FONT COLOR="#ff3333"><b>0508 Електроніка</b></font><br><br>



<TABLE WIDTH=507 BORDER=1 BORDERCOLOR="#00b8ff" CELLPADDING=4 CELLSPACING=3>
									<COL WIDTH=74>
									<COL WIDTH=406>
									<THEAD>
										<TR VALIGN=TOP>
											<TD ROWSPAN=4 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B><SPAN STYLE="font-style: normal"><FONT FACE="Arial, sans-serif"><FONT COLOR="#ff3333">6.050801</FONT></FONT></SPAN></B></FONT></FONT></FONT></P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>Мікро- та наноелектроніка</B></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.56cm; margin-right: 0.01cm; text-indent: -0.32cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.05080101 
												&mdash;  Мікро- та наноелектронні прилади і пристрої</FONT></FONT></FONT></P>
											</TD>
										</TR>
										
									</THEAD>
									<TBODY>
										<TR VALIGN=TOP>
											<TD ROWSPAN=3 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>6.050802</B></FONT></FONT></FONT></P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>Електронні пристрої та системи</B></FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.62cm; margin-right: 0.01cm; text-indent: -0.29cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.05080201  
												&mdash; Електронні прилади і пристрої</FONT></FONT></FONT></P>
											</TD>
										</TR>
										
									</TBODY>
								</TABLE>
-->

<br>По цих спеціальностях також здійснюється підготовка фахівців з поглибленим вивченням іноземної мови на рівні технічного перекладача.<br><br>


<!--

<b><center>Радіотехнічний напрямок</center></b><br>

<b>"Радіотехніка"</b><br>
Радіоінженери займаються розробкою та експлуатацією сучасної аудіо- та відео- побутової техніки, системи передавання та приймання інформації, а також радіотехнічних систем спеціального призначення. <br><br>

<b>"Апаратура радіозв'язку, радіомовлення і телебачення"</b><br>
Інженери-радисти цієї спеціальності можуть працювати на підприємствах і в організаціях, які займаються розробкою та модернізацією засобів радіозв'язку і телерадіомовлення, їх експлуатацією та ремонтом. <br><br>

<b><center>Напрямок радіоелектронних апаратів</center></b><br>

<b>"Виробництво електронних засобів"</b><br>
Студенти цієї спеціальності вивчають сучасні засоби автоматизованого проектування, конструювання та виробництва електронно-обчислювальних та мікропроцесорних пристроїв для всіх галузей промисловості та сільського господарства. <br><br>

<b>"Біотехнічні та медичні апарати і системи"</b><br>
Випускники цієї спеціальності працюють в медичних та санітарно-курортних установах, державних та приватних фірмах як в Україні так і за її межами і займаються розвитком комп'ютерної томографії та автоматизованих систем диспансерного контролю, проблемами мікропроцесорного управління вимірюваннями для представлення лікарю інформації в обробленому вигляді. <br><br>

<b><center>Напрямок телекомунікацій</center></b><br>

<b>"Телекомунікаційні системи та мережі"</b><br>
Інженери телекомунікацій займаються розробкою, модернізацією, обслуговуванням та експлуатацією засобів стільникового, пейджингового, транкового та космічного зв'зку, автоматичних телефонних станцій відомчого, міського, міжміського та міжнародного зв'язку, кабельних, супутникових, радіорелейних та волоконно-оптичних мереж зв'язку.<br><br>

<b>"Технології та засоби телекомунікацій"</b><br>
Інженери електронного приладобудування цієї спеціальності працюють в державних та комерційних установах, займаються розробкою, експлуатацією та ремонтом цифрових систем передачі, комп'ютерних мереж, мобільних систем зв'язку, систем телеметрії та дистанційного керування, системи кабельного та супутникового телебачення.<br><br>

<b>"Оргтехніка та зв'язок"</b><br>
Підготовка інженерів-телекомунікаційників даної спеціалізації здійснюється лише у ВДТУ. Спеціалісти цього фаху займаються розробкою та обслуговуванням копіювальних і друкувальних пристроїв, торгівельної та банківської техніки, систем електронної пошти та засобів телефонного та документального електрозв'язку, персональних комп'ютерів та мережі "Internet".<br><br>

-->

</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
