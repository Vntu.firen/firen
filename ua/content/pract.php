<?php
	$title = "Інтерактивна розрахункова робота";
?>
<META content="text/html; charset=windows-1251" http-equiv=Content-Type>
<!-- <link href="content/pract/application-style.css" media="screen" rel="stylesheet" type="text/css"> -->
<script src="content/pract/application-main.js" type="text/javascript"></script>
<body class="articles show">
<div id="content-main">
<h2>
Системы беспроводного широкополосного <br> доступа СБШД
</h2>
интерактивная расчётная работа

<div>
<script type="text/javascript" src="content/pract/fresnel.js" language="JavaScript"></script>
<script language="JavaScript" src="content/pract/fsl2.js" type="text/javascript"></script>
<script language="JavaScript" src="content/pract/angle.js" type="text/javascript"></script>
<script language="JavaScript" src="content/pract/coverage.js" type="text/javascript"></script>
<script language="JavaScript" src="content/pract/dbm.js" type="text/javascript"></script>
<div class="accordion ui-accordion ui-widget ui-helper-reset" role="tablist">
<div>
<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" role="tab" id="ui-accordion-1-header-0" aria-controls="ui-accordion-1-panel-0" aria-selected="false" tabindex="0"><a href="">Расчет энергетического бюджета</a></h3>

<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-accordion-1-panel-0" aria-labelledby="ui-accordion-1-header-0" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><a href="" target="_blank"><img alt="калькулятор расчет энергетического бюджета трассы" src="content/pract/content_energy.gif" style="height: 180px; width: 470px;"></a>

<div>Данный калькулятор позволяет вычислить энергетический бюджет беспроводной трассы и получить ответы на следующие вопросы:</div>

<ul>
	<li>возможна ли связь при помощи радиомаршрутизаторов на заданном расстоянии?</li>
	<li>какие антенны для этого потребуются?</li>
	<li>какая скорость в канале может быть достигнута?</li>
</ul>

<p>Результат расчета - <strong>запас по энергетике</strong>, который должен составлять <strong>не менее 20dB</strong> для сохранения устойчивой связи при резких ухудшениях условий прохождения радиоволн.</p>

<form action="" method="post" name="margin">
<table border="0" cellpadding="3" cellspacing="3" class="block_content" style="width:470px">
	<tbody>
		<tr>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Рабочая частота</strong></p>
			</td>
			<td colspan="3" style="background-color:rgb(102, 153, 204)">
			<p><strong>Расстояние между точками</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="7" name="txtfsll_Freq" size="7" type="text" value="5000"> (MHz)<br>
			&nbsp;</td>
			<td colspan="3"><input maxlength="7" name="txtfsll_Dist" size="7" type="text" value="10"> (км)<br>
			&nbsp;</td>
		</tr>
		<tr>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Мощность передатчика</strong></p>
			</td>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Усиление антенны передатчика</strong></p>
			</td>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Потери в кабеле и разъемах 1</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="5" name="txtTXpwr" size="5" type="text" value="17"> (dBm)</td>
			<td><input maxlength="5" name="txtTXantgain" size="5" type="text" value="24"> (dBi)</td>
			<td><input maxlength="5" name="txtTXcabloss" size="5" type="text" value="1.5"> (dB)<br>
			&nbsp;</td>
		</tr>
		<tr>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Чувствительность приемника</strong></p>
			</td>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Усиление антенны приемника</strong></p>
			</td>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Потери в кабеле и разъемах 2</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="5" name="txtRXsens" size="5" type="text" value="-85"> (dBm)</td>
			<td><input maxlength="5" name="txtRXantgain" size="5" type="text" value="24"> (dBi)</td>
			<td><input maxlength="5" name="txtRXcabloss" size="5" type="text" value="1.5"> (dB)</td>
		</tr>
		<tr>
			<td colspan="3" style="background-color:rgb(102, 153, 204)">
			<p><strong>Результаты рассчета:</strong></p>
			</td>
		</tr>
		<tr>
			<td colspan="3"><input id="cmdSOM" language="javascript" name="cmdSOM" onclick="return cmdSOM_onclick()" type="button" value="Рассчитать запас сигнала"></td>
		</tr>
		<tr>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Потери в свободном пространстве</strong></p>
			</td>
			<td style="background-color:rgb(102, 153, 204)">
			<p><strong>Уровень сигнала на входе приемника</strong></p>
			</td>
			<td style="background-color:rgb(102, 153, 153); text-align:left; vertical-align:top">
			<p><strong>Запас по энергетике канала</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="6" name="txtFSL" size="6" type="text" value=""> (dB)</td>
			<td><input maxlength="6" name="txtRXsig" size="6" type="text" value=""> (dBm)</td>
			<td><input maxlength="6" name="txtSOM" size="6" type="text" value=""> (dB)<br>
			&nbsp;</td>
		</tr>
	</tbody>
</table>
</form>

<hr>

<div>&nbsp;</div>

<div>Справочная информация, необходимая для расчета:</div>

<div>&nbsp;</div>

<div>
<table border="1" cellpadding="5" cellspacing="0" class="block_content" style="border-collapse:separate; height:452px; width:470px">
	<tbody>
		<tr>
			<td style="background-color:rgb(204, 204, 204)">
			<div><strong>Скорость в канале, Мбит/с</strong></div>
			</td>
			<td style="background-color:rgb(204, 204, 204)">
			<div><strong>Чувствит. приемника порог, dBm</strong></div>
			</td>
			<td style="background-color:rgb(204, 204, 204)">
			<div><strong>Мощность передатчика PA63</strong><strong>, dBm</strong></div>
			</td>
			<td style="background-color:rgb(204, 204, 204)">
			<div><strong>Мощность передатчика</strong></div>

			<div><span style="color:#FF0000"><strong>PA400</strong></span><strong>, dBm</strong></div>
			</td>
			<td style="background-color:rgb(204, 204, 204)">
			<div>
			<div><strong>Мощность передатчика</strong></div>

			<div><span style="color:#FF0000"><strong>PA600</strong></span><strong>, dBm</strong></div>
			</div>
			</td>
		</tr>
		<tr>
			<td>
			<div><em><strong>Нормальный режим, ofdm</strong></em></div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>&nbsp;</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>54</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–70</div>
			</td>
			<td>
			<div>14 +/-2 dBm</div>
			</td>
			<td>
			<div>
			<div>21 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>23 +/-1.5 dBm</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>48</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–73</div>
			</td>
			<td>
			<div>15 +/-2 dBm</div>
			</td>
			<td>
			<div>
			<div>22 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>24 +/-1.5 dBm</div>
			</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>36</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–78</div>
			</td>
			<td>
			<div>16 +/-2 dBm</div>
			</td>
			<td>
			<div>
			<div>24 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>26 +/-1.5 dBm</div>
			</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>24</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–82</div>
			</td>
			<td>
			<div>
			<div>17 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>26 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>28 +/-1.5 dBm</div>
			</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>18</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–85</div>
			</td>
			<td>
			<div>
			<div>17 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>26 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>28 +/-1.5 dBm</div>
			</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>12</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–87</div>
			</td>
			<td>
			<div>
			<div>17 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>26 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>28 +/-1.5 dBm</div>
			</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>
			<div>9</div>
			</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–88</div>
			</td>
			<td>
			<div>
			<div>17 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>26 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>28 +/-1.5 dBm</div>
			</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>
			<div>
			<div>6</div>
			</div>
			</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>
			<div>–90</div>
			</div>
			</td>
			<td>
			<div>
			<div>18&nbsp; +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>
			<div>26 +/-2 dBm</div>
			</div>
			</td>
			<td>
			<div>28 +/-1.5 dBm</div>
			</td>
		</tr>
		<tr>
			<td style="background-color:rgb(204, 204, 204); vertical-align:top; width:20%">
			<div>
			<div><em><strong>Режим ССК:</strong></em></div>
			</div>
			</td>
			<td style="background-color:rgb(204, 204, 204); vertical-align:top; width:20%">
			<div>&nbsp;</div>
			</td>
			<td style="background-color:rgb(204, 204, 204); vertical-align:top; width:20%">
			<div>&nbsp;</div>
			</td>
			<td style="background-color:rgb(204, 204, 204); vertical-align:top; width:20%">
			<div>&nbsp;</div>
			</td>
			<td style="background-color:rgb(204, 204, 204); vertical-align:top; width:20%">
			<div>&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>11</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–88</div>
			</td>
			<td>
			<div>19 +/-2 dBm</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>5.5</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–90</div>
			</td>
			<td>
			<div>19 +/-2 dBm</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>
			<div>2</div>
			</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>–92</div>
			</td>
			<td>
			<div>19 +/-2 dBm</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>1</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:20%">
			<div>-93</div>
			</td>
			<td>
			<div>19 +/-2 dBm</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
			<td>
			<div>&nbsp;</div>
			</td>
		</tr>
	</tbody>
</table>
</div>

<div><strong>&nbsp;</strong></div>

<div><strong>Усиление антенны для моделей&nbsp; с интегрированной направленной антенной:</strong></div>

<div><strong>&nbsp;</strong></div>

<div>
<table border="1" bordercolor="#000000" cellpadding="3" cellspacing="0" height="106" style="border-collapse: separate;" width="437">
	<tbody>
		<tr>
			<td style="background-color:rgb(204, 204, 204)">
			<div style="text-align: center;"><strong>Диапазон, ГГц</strong></div>
			</td>
			<td style="background-color:rgb(204, 204, 204)">
			<div style="text-align: center;"><strong>Усиление антенны, dbi</strong></div>
			</td>
			<td style="background-color: rgb(204, 204, 204); text-align: center;"><strong>Диаграмма направленности, градусы</strong></td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:50%">
			<div>4.9 - 6.075</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:50%">
			<div>24 +/- 1.5</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:50%">9х9</td>
		</tr>
		<tr>
			<td style="text-align:center; vertical-align:top; width:50%">
			<div>2.3 - 2.5</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:50%">
			<div>18 +/- 1.5</div>
			</td>
			<td style="text-align:center; vertical-align:top; width:50%">19х19</td>
		</tr>
	</tbody>
</table>
</div>

<div><strong>&nbsp;</strong></div>

<div><strong>Примечания:</strong></div>

<div><strong>&nbsp;</strong></div>

<ul>
	<li style="text-align:justify">Предполагается, что антенны приемника и передатчика находятся в зоне прямой видимости.</li>
	<li style="text-align:justify">Энергетический расчет не учитывает дифракцию Френеля на препятствиях, находящихся поблизости от пути распространения сигнала. Определить, какое пространство должно быть свободным вокруг воображаемой оси между антеннами можно при помощи калькулятора, расчитывающего радиус зоны Френеля.</li>
	<li style="text-align:justify">Чувствительность приемника и мощность передатчика в зависимости от желаемой скорости в канале вы найдете в таблице.</li>
	<li style="text-align:justify">Потери в ВЧ кабеле зависят от его длины и частоты передаваемого сигнала.</li>
	<li style="text-align:justify">Потери в ВЧ разъемах обычно составляют не более 0.5 - 1dB</li>
	<li style="text-align:justify">Наличие помех резко ухудшает качество связи даже при достаточном уровне сигнала</li>
</ul>
</div>
</div>

<div><!-- NEXT -->
<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" role="tab" id="ui-accordion-1-header-1" aria-controls="ui-accordion-1-panel-1" aria-selected="false" tabindex="-1"><a href="">Расчет радиуса зоны Френеля</a></h3>

<div class="content ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" style="padding: 10px; display: none;" id="ui-accordion-1-panel-1" aria-labelledby="ui-accordion-1-header-1" role="tabpanel" aria-expanded="false" aria-hidden="true"><img alt="калькулятор  зона Френеля" src="content/pract/content_fresnel1.gif" style="height:215px; width:470px">
<p style="text-align:justify"><a name="fren"></a>Радиоволна в процессе распространения в пространстве занимает объем в виде эллипсоида вращения с максимальным радиусом в середине пролета, который называют зоной Френеля. Естесственные (земля, холмы, деревья) и искуственные (здания, столбы) преграды, попадающие в это пространство ослабляют сигнал.</p>

<div style="text-align: justify;">Радиус 1й зоны Френеля в самой широкой части может быть расчитан при помощи этого калькулятора. Здесь <strong><em>d</em></strong> это длина линка в километрах, <strong><em>f</em></strong> это частота в ГГц, а <strong><em>r</em></strong> - радиус зоны Френеля в метрах.</div>

<p style="text-align:justify">Замечания:</p>

<ol>
	<li>
	<p style="text-align:justify">Обычно блокирование 20% зоны Френеля вносит незначительное затухание в канал. Свыше 40% затухание сигнала будет уже значительным, следует избегать попадания препятствий на пути распространения.</p>
	</li>
	<li>
	<p style="text-align:justify">Этот расчет сделан в предположении что земля <em>плоская</em>. Он не учитывает кривизну земной поверхности. Для протяженных каналов (более 25 км) следует проводить совокупный расчет, учитывающий рельев местности и естесственные преграды на пути распространения. В случае протяженных линков следует стараться увеличивать высоту подвеса антенн, принимая во внимание кривизну земной поверхности.</p>
	</li>
</ol>

<form action=" " method="get" name="main">
<table border="0" cellpadding="3" cellspacing="2" class="block_content">
	<tbody>
		<tr>
			<td>
			<p><strong>Расстояние в Км.</strong></p>
			</td>
			<td>
			<p><strong>Частота в ГГц</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="7" name="txtDistance" size="7" type="text" value="10"> (Километров)<br>
			&nbsp;</td>
			<td><input maxlength="7" name="txtFreq" size="7" type="text" value="5.8"> (ГГц) <input language="javascript" name="cmdfresnel" onclick="return calcFresnel()" type="button" value="Считать"><br>
			&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
			<p><strong>Результаты:</strong></p>
			</td>
		</tr>
		<tr>
			<td>
			<p>Первая зона Френеля<br>
			<input maxlength="6" name="txtAnsFresZone1" size="6" type="text" value=""><br>
			метров.</p>
			</td>
			<td colspan="2">
			<p>80% первой зоны Френеля<br>
			<input maxlength="6" name="txtAnsFresZone2" size="6" type="text" value=""><br>
			метров.</p>
			</td>
		</tr>
	</tbody>
</table>
</form>
</div>
</div>

<div>
<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" role="tab" id="ui-accordion-1-header-2" aria-controls="ui-accordion-1-panel-2" aria-selected="false" tabindex="-1"><a href="http://www./sections/4/articles/28#">Расчет радиусов покрытия секторной антенны</a></h3>

<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-accordion-1-panel-2" aria-labelledby="ui-accordion-1-header-2" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><img alt="калькулятор радиус покрытия секторной антенны" src="content/pract/content_cell.gif" style="height:250px; width:467px">
<div style="text-align: justify;">Этот расчет позволит подобрать антенну с нужной шириной луча в вертикальной плоскости, а также вычислить угол ее наклона относительно горизонта исходя из желаемого внешнего и внутреннего радиусов обслуживания.</div>

<p>&nbsp;</p>

<form action="http://www./sections/4/articles/(Empty%20Reference!)" method="get" name="coverage">
<table border="0" cellpadding="3" cellspacing="2" class="block_content" style="width:470px">
	<tbody>
		<tr>
			<td>
			<p><strong>Высота антенны</strong></p>
			</td>
			<td>
			<p><strong>Угол наклона</strong></p>
			</td>
			<td>
			<p><strong>Ширина луча</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="5" name="txtHgt" size="7" type="text" value="50"> метров<br>
			&nbsp;</td>
			<td><input maxlength="7" name="txtDowntilt" size="7" type="text" value="5"> градусов<br>
			&nbsp;</td>
			<td><input maxlength="7" name="txtBeamwidth" size="7" type="text" value="10"> градусов<br>
			&nbsp;</td>
		</tr>
		<tr>
			<td>
			<p><strong>Рассчитать расстояние</strong></p>
			</td>
			<td>
			<p><strong>Внутренний радиус</strong></p>
			</td>
			<td>
			<p><strong>Внешний радиус</strong></p>
			</td>
		</tr>
		<tr>
			<td><input language="javascript" name="cmdcaldist" onclick="return calcCoverage()" type="button" value="Считать"></td>
			<td><input maxlength="25" name="txtAnsInnerRad" size="7" type="text" value=""> километров</td>
			<td><input maxlength="25" name="txtAnsOuterRad" size="7" type="text" value=""> километров</td>
		</tr>
	</tbody>
</table>
</form>

<p>&nbsp;</p>
</div>
</div>

<div><!-- Расчет радиусов покрытия секторной антенны --><!-- NEXT -->
<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-corner-all" role="tab" id="ui-accordion-1-header-3" aria-controls="ui-accordion-1-panel-3" aria-selected="false" tabindex="-1"><a href="http://www./sections/4/articles/28#">Расчет необходимого наклона антенны БС</a></h3>

<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom" id="ui-accordion-1-panel-3" aria-labelledby="ui-accordion-1-header-3" role="tabpanel" aria-expanded="false" aria-hidden="true" style="display: none;"><img alt="калькулятор расчет наклона антенны" src="content/pract/content_downtilt.gif" style="height:242px; width:469px">
<div style="text-align: justify;">Данный несложный расчет позволяет вычислить необходимый угол наклона антенны базовой станции <strong>БС</strong> относительно линии горизонта, зная высоту точек установки антенны <strong>БС</strong> и абонентской станции <strong>АС</strong>.</div>

<p>&nbsp;</p>

<form action="" method="get" name="angle">
<table border="0" cellpadding="3" cellspacing="2" class="block_content">
	<tbody>
		<tr>
			<td colspan="2">
			<p><strong>Высота подвеса БС, Hb</strong></p>
			</td>
			<td>
			<p><strong>Высота подвеса АС, Hr</strong></p>
			</td>
		</tr>
		<tr>
			<td colspan="2"><input maxlength="7" name="txtBaseHgt" size="7" type="text" value="50"> метров<br>
			&nbsp;</td>
			<td><input maxlength="7" name="txtRemoteHgt" size="7" type="text" value="15"> метров<br>
			&nbsp;</td>
		</tr>
		<tr>
			<td colspan="3">
			<p><strong>Рассчитать расстояние</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="7" name="txtAngle" size="5" type="text" value="4"><br>
			угол наклона,°</td>
			<td><input language="javascript" name="cmdcaldist" onclick="return calcDist()" type="button" value="Считать"></td>
			<td><input maxlength="25" name="txtAnsDistance" size="5" type="text" value=""> километров</td>
		</tr>
		<tr>
			<td colspan="3">
			<p><strong>Рассчитать угол наклона по расстоянию</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="7" name="txtDistance" size="5" type="text" value="1"><br>
			расстояние, километров</td>
			<td><input language="javascript" name="cmdcalangle" onclick="return calcAngle()" type="button" value="Считать"></td>
			<td><input maxlength="25" name="txtAnsAngle" size="5" type="text" value=""> угол наклона,°</td>
		</tr>
	</tbody>
</table>
</form>
</div>
<!-- Расчет необходимого наклона антенны БС --><!-- NEXT --></div>

<div>
<h3 class="ui-accordion-header ui-helper-reset ui-state-default ui-accordion-header-active ui-state-active ui-corner-top" role="tab" id="ui-accordion-1-header-4" aria-controls="ui-accordion-1-panel-4" aria-selected="true" tabindex="0"><a href="">Перевод dBm в mW и наоборот</a></h3>

<div class="ui-accordion-content ui-helper-reset ui-widget-content ui-corner-bottom ui-accordion-content-active" id="ui-accordion-1-panel-4" aria-labelledby="ui-accordion-1-header-4" role="tabpanel" aria-expanded="true" aria-hidden="false" style="display: block;">
<div>Справка:</div>

<div>&nbsp;</div>

<p>1000мВт = 1Вт = 1000mW; 1dBm = 1дБм</p>

<p>&nbsp;</p>

<p>Из мВт в дБм = 10Log<sub>10</sub>(mW)</p>

<p>Из дБм в мВт = 10<sup>(dBm/10)</sup></p>
&nbsp;

<form action="" method="get" name="calcdbm">

<table border="0" cellpadding="3" cellspacing="2" class="block_content">
	<tbody>
		<tr>
			<td>
			<p><strong>Мощность в mW</strong></p>
			</td>
			<td>
			<p><strong>Результат в dBm</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="7" name="dbmmw" size="7" type="text" value="10"></td>
			<td><input maxlength="7" name="rdbm" size="7" type="text" value=""> <input language="javascript" name="dbmcalc" onclick="document.calcdbm.rdbm.value = to_dbm(document.calcdbm.dbmmw.value); return(0);" type="button" value="Считать"></td>
		</tr>
		<tr>
			<td>
			<p><strong>Мощность в dBm</strong></p>
			</td>
			<td>
			<p><strong>Результат в mW</strong></p>
			</td>
		</tr>
		<tr>
			<td><input maxlength="7" name="udbm" size="7" type="text" value="10"></td>
			<td><input maxlength="7" name="umw" size="7" type="text" value=""> <input language="javascript" name="mwcalc" onclick="document.calcdbm.umw.value = from_dbm(document.calcdbm.udbm.value); return(0);" type="button" value="Считать"></td>
		</tr>
	</tbody>
</table>
</form>
</div>
<!-- Перевод dBm в mW и наоборот --></div>
</div>

<p><!-- accordion --></p>

<p style="text-align: justify;">&nbsp;</p>

<p style="text-align: justify;">Для выполнения расчётной работы по дисциплине СБШД согласно заданиям используются следующие калькуляторы для расчёта и проектировании беспроводных сетей:</p>

<ul>
	<li style="text-align: justify;"><strong>Калькулятор расчёта энергетического бюджета</strong>: отвечает на наиболее часто встречающийся в тематике беспроводной связи вопрос: возможна ли связь на интересующем расстоянии и каково её качество, а так же позволяет подобрать необходимые модели радиомаршрутизаторов и антенн для рассматриваемой дистанции;</li>
	<li style="text-align: justify;"><strong>Калькулятор расчёта радиуса зоны Френеля</strong>: позволяет рассчитать минимальную высоту подвеса радиооборудования для обеспечения качественной радиосвязи;</li>
	<li style="text-align: justify;"><strong>Калькулятор расчета радиусов покрытия секторной антенны</strong>: позволяет узнать площадь покрываемой радиолучом территории в зависимости от угла наклона и ширины луча устанавливаемой антенны;</li>
	<li style="text-align: justify;"><strong>Калькулятор расчёта необходимого угла наклона антенны базовой станции</strong>: позволяет рассчитать оптимальные условия связи в зависимости от высот подвеса антенн и расстояния между базовой и клиентской точками;</li>
	<li style="text-align: justify;"><strong>Перевод </strong><strong>dBm в mW</strong>: калькулятор позволяет быстро осуществить перевод dBm в mW&nbsp; и обратно, поскольку мощность (в том числе и мощность радиосигнала) может быть измерена обеими величинами, и децибелами и милливаттами.</li>
</ul>

</div>

</div>




