<?php
	$title = "EUROPEAN CREDIT TRANSFER SYSTEM";
?>

                        <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>


<font class=krsnbold>Освітньо-кваліфікаційний рівень  - БАКАЛАВР</font> <br><br>

<b>ECTS - інформаційний пакет студенту спеціальності телекомунікаційні системи та мережі</b><br><br>
Сторінок: 197<br>
Мова: українська<br>
Об'єм: 5.4 МБ<br>
<br>
Завантажити: <a class=ssilk href=../files/rt_bak.doc>DOC</a> | <a class=ssilk href=../files/rt_bak.pdf>PDF</a> | <a class=ssilk href=../files/rt_bak.odt>ODT</a> | <a class=ssilk href=../files/rt_bak.djvu>DJVU</a><br><br>
<br>
<b>ECTS - інформаційний пакет студенту спеціальності радіотехніка</b><br><br>
Сторінок: 234<br>
Мова: українська<br>
Об'єм: 3.2 МБ<br>
<br>
Завантажити: <a class=ssilk href=../files/tsm_bak.doc>DOC</a> | <a class=ssilk href=../files/tsm_bak.pdf>PDF</a> | <a class=ssilk href=../files/tsm_bak.odt>ODT</a> | <a class=ssilk href=../files/tsm_bak.djvu>DJVU</a><br><br>

<br><br>
<font class=krsnbold>Освітньо-кваліфікаційний рівень - СПЕЦІАЛІСТ</font> <br><br>

<b>ECTS - інформаційний пакет студенту спеціальності Апаратура радіозв'язку, радіомовлення і телебачення; Оргтехніка та зв'язок</b><br><br>
Сторінок: 92<br>
Мова: українська<br>
Об'єм: 1.5 МБ<br>
<br>
Завантажити: <a class=ssilk href=../files/rz_spec.doc>DOC</a> | <a class=ssilk href=../files/rz_spec.pdf>PDF</a> | <a class=ssilk href=../files/rz_spec.odt>ODT</a>  | <a class=ssilk href=../files/rz_spec.djvu>DJVU</a><br><br><br>

<b>ECTS - інформаційний пакет студенту спеціальності телекомунікаційні системи та мережі</b><br><br>
Сторінок: 197<br>
Мова: українська<br>
Об'єм: 5.4 МБ<br>
<br>
Завантажити: <a class=ssilk href=../files/tsm_spec.doc>DOC</a> | <a class=ssilk href=../files/tsm_spec.pdf>PDF</a> | <a class=ssilk href=../files/tsm_spec.odt>ODT</a> | <a class=ssilk href=../files/tsm_spec.djvu>DJVU</a><br><br><br>

<b>ECTS - інформаційний пакет студенту спеціальності радіотехніка</b><br><br>
Сторінок: 96<br>
Мова: українська<br>
Об'єм: 1.4 МБ<br>
<br>
Завантажити: <a class=ssilk href=../files/rt_spec.doc>DOC</a> | <a class=ssilk href=../files/rt_spec.pdf>PDF</a> | <a class=ssilk href=../files/rt_spec.odt>ODT</a> | <a class=ssilk href=../files/rt_spec.djvu>DJVU</a><br><br><br>

<br>
<font class=krsnbold>Освітньо-кваліфікаційний рівень - МАГІСТР ІНЖНЕРІЇ</font> <br><br>

<b>ECTS - інформаційний пакет студенту спеціальності Апаратура радіозв'язку, радіомовлення і телебачення; Оргтехніка та зв'язок</b><br><br>
Сторінок: 92<br>
Мова: українська<br>
Об'єм: 1.5 МБ<br>
<br>
Завантажити:<a class=ssilk href=../files/rz_mi.doc>DOC</a> | <a class=ssilk href=../files/rz_mi.pdf>PDF</a> | <a class=ssilk href=../files/rz_mi.odt>ODT</a> | <a class=ssilk href=../files/rz_mi.djvu>DJVU</a><br><br>

<br>
<b>ECTS - інформаційний пакет студенту спеціальності радіотехніка</b><br><br>
Сторінок: 101<br>
Мова: українська<br>
Об'єм: 1.5 МБ<br>
<br>
Завантажити: <a class=ssilk href=../files/rt_mi.doc>DOC</a> | <a class=ssilk href=../files/rt_mi.pdf>PDF</a> | <a class=ssilk href=../files/rt_mi.odt>ODT</a> | <a class=ssilk href=../files/rt_mi.djvu>DJVU</a><br><br>
 <br><br>

 <br><br>

</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
