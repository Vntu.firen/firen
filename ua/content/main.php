<?php
	$title = "ФІРЕН ГОЛОВНА СТОРІНКА";
?>

                        <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>

                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT class=content><center><b>
<!-- <img src=img/ny_2015.jpg width=500 border=0><br><br> 
ОГОЛОШЕННЯ<br><br>
Випускники ФРТЗП 2015 року можуть отримати дипломи і додатки в деканаті<br><br>
---------------<br><br>
-->

<!--
16 листопада - День працівників радіо, телебачення та зв'язку!<br>
Історія свята розпочалася 16 листопада 1924 року, коли у Харкові в ефір вийшла перша в Україні радіопрограма.<br>
В 2018 році ми святкуємо 94 річницю цієї події!<br><br>
-->

<!--
7 травня - день РАДІО!<br><br>
Цього дня у 1895 році видатний вчений Олександр Степанович ПОПОВ вперше в світі продемонстрував систему радіозв'язку та провів сеанс передачі радіоповідомлення.<br><br><br>
-->

<!--<h2> З Новим 2017 роком! </h2><br>
<img src=img/frtzp_new_year.jpeg alt='ФІРЕН ВНТУ' title='З Новим 2017 роком!' border=0 width=500>
<br><br>
....&nbsp;.-&nbsp;.--.&nbsp;.--.&nbsp;-.--&nbsp;&nbsp;&nbsp;-.&nbsp;.&nbsp;.--&nbsp;&nbsp;&nbsp;-.--&nbsp;.&nbsp;.-&nbsp;.-.&nbsp;&nbsp;&nbsp;..-.&nbsp;..&nbsp;.-.&nbsp;.&nbsp;-.
<br><br>
-->

<table border=1 class="c11"><tbody><tr class="c3"><td class="c12" colspan="2" rowspan="1"><p class="c7 c2"><span class="c0"></span></p><p class="c7"><span class="c0">Факультет інфокомунікацій, радіоелектроніки та наносистем</span></p><p class="c2 c7"><span class="c0"></span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1"><span class="c0">15 Автоматизація та приладобудування</span></p><p class="c1 c2"><span class="c0"></span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">153 Мікро- та наносистемна техніка</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1"><span class="c0">16 Хімічна та біоінженерія</span></p><p class="c1 c2"><span class="c0"></span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">163 Біомедична інженерія</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1"><span class="c0">17 Електроніка та телекомунікації</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">171 Електроніка</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="1"><p class="c1 c2"><span class="c0"></span></p><p class="c1"><span class="c0"> </span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">електронні пристрої та  системи</span></p></td></tr><tr class="c3"><td class="c5" colspan="1" rowspan="7"><p class="c1"><span class="c0">17 Електроніка та телекомунікації</span></p></td><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">172 Телекомунікації та радіотехніка</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">телекомунікації </span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">інформаційно-телекомунікаційні технології</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">телекомунікаційні системи та мережі</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">радіотехніка</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">інформаційно-обчислювальні засоби радіоелектронних систем</span></p></td></tr><tr class="c3"><td class="c4" colspan="1" rowspan="1"><p class="c1"><span class="c0">інтелектуальні технології мікросистемної радіоелектронної техніки</span></p></td></tr></tbody></table>

<p><center>

</CENTER>
</td></tr>

                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>
Факультет інфокомунікацій, радіоелектроніки та наносистем створено 1 грудня 2016 р. Раніше - Факультет радіотехніки, зв’язку та приладобудування. ФРТЗП утворено в 2015 році шляхом реорганізації 
Інституту радіотехніки, зв’язку та приладобудування (ІнРТЗП), який було створено в 2002 році на базі факультету 
радіоелектроніки (ФРЕ), одного з найстаріших факультетів нашого університету.
<br><br>
       На факультеті зараз працює 7 професорів i понад 40 доцентiв, серед них один лауреат державної премії СРСР.  
В ФРТЗП сьогодні навчається більше тисячі студентів, працює 6 кафедр та два
компьютерних центри під’єднані до мережі Інтернет, де працює близько 100 викладачів, наукових 
співробітників та навчально-допоміжного персоналу. ФРТЗП здійснює підготовку бакалаврів, інженерів та 
магістрів по денній та заочній формі навчання з 5-ти напрямків, 7-ми спеціальностей та 2-х спеціалізацій. 
За всіма спеціальностями також готуються фахівці з поглибленим вивченням іноземної мови на рівні технічного
 перекладача.
<br><br>
<img src=img/inrtzp1.jpg border=0>
<br><br>
       Про високу якість професійної підготовки, яку забезпечує ФРТЗП засвідчує той факт, що багато його 
випускників займають ключові адміністративні та науково-технічні посади в різних відомствах, організаціях, 
провідних телекомунікаційних та радіоелектронних підприємствах і фірмах. Достатньо назвати в цьому 
контексті О.І.Сугоняка голову асоціації комерційних банків України;  В.О. Поджаренка професора, доктора наук; 
завідувача кафедри ВНТУ; Р.Н.Квєтного професора, доктора наук, завідувача кафедри ВНТУ;
С.М.Злепка професора, доктора наук, заступника директора ФРТЗП ВНТУ; Ю.О.Коломійця директора 
Вінницької філії “Київстар”; О.С.Пізняка генерального директора компанії “Техногаз”; 
В.В.Скомаровського президента фірми “Владімір”;В.В.Терліковського директора заводу “Маяк”; 
О.І.Козака директора Вінницького обласного радіотелевізійного передавального центру; 
Л.Ф.Мінова головного інженера Центру експлуатації первинної мережі зв’язку ВАТ “Укртелеком”.
<br><br>
       На факультеті працюють провідні вчені, імена яких відомі як і в Україні, так і за її межам. Це доктор технічних 
наук, професор, академік  Академії інженерних наук України, голова міжнародного координаційного центру 
з напрямку “Негатроніка” М.А.Філинюк; лауреат Державної премії СРСР  С.Т.Барась; неодноразовий учасник 
міжнародних виставок винаходів, доктор технічних наук О.В.Осадчук, який у 2006 році на міжнародній виставці 
у Будапешті отримав золоту медаль і найвищу нагороду виставки Гран-Прі.
<br><br>
<img src=img/inrtzp2.jpg border=0>
<br><br>
<!--
Факультет радіотехніки, зв'язку та приладобудування (ФРТЗП) Вінницького національного технічного університету зднійснює підготовку фахівців на денній та заочній формі навчання за такими напрямами, спеціальностями та спеціалізаціями<br><br>
	<b>	0509 Радіотехніка, приладобудування та зв’язок<br><br>	</b>
    6.050901  Радіотехніка<br>
            	  6.050901-01 Радіотехніка<br>
               6.050901-02 Апаратура радіозв'язку, радіомовлення і телебачення <br>
<br>               
      6.050902  Радіоелектронні апарати<br>
	            6.050902-01 Радіоелектронні апарати і засоби<br>
	            6.050902-02 Біотехнічні та медичні апарати і системи<br>
							<br>
      6.050903  Телекомунікації<br>
	          6.050903-02 Телекомунікаційні системи та мережі<br>
		  6.050903-03 Технології та засоби телекомунікацій<br>
             	  6.050903-04 Оргтехніка та звя'зок<br>

<br><br>
	<b>0508 Електроніка<br><br>	</b>
		6.050801 Мікро- та наноелектроніка<br>
		6.05080101 Мікро- та наноелектронні прилади і пристрої<br>
<br>
		6.050802 Електронні пристрої та системи<br>
		6.05080201 Електронні прилади і пристрої<br>
<br><br>
--> 

<!--  <center>
<script type="text/javascript">
<!--
// ** Copyright 1998 G4VWL  right.event added 2003
function click() {
  if (event.button==2) {
   alert('This site and its contents are (c) copyright. G4VWL 2003\n' +
         'but please feel free to view the source code.');
   }
} 
document.onmouseup=click;
function setup() {
//            The following is just Morse Code for the Alphabet!
   cw = new Array(" ",".-","-...","-.-.","-..",".","..-.","--.",
                  "....","..",".---","-.-",".-..","--","-.",
                  "---",".--.","--.-",".-.","...","-","..-","...-",
                  ".--","-..-","-.--","--..",
                  "-----",".----","..---","...--","....-",".....",
                  "-....","--...","---..","----.",".-.-.-",
                  "..--..","-.--.",".-.-.","--..--","-....-"," ")
   alpha = new Array(" ","A","B","C","D","E","F","G","H","I","J",
                    "K","L","M","N","O","P","Q","R","S","T","U",
                    "V","W","X","Y","Z","0","1","2","3","4","5",
                    "6","7","8","9",".","?","kn","ar",",","-"," ")
}
function alpha2cw() {
   m = "";
// Read Text window and convert to upper case...
   t1 = document.form.text.value;
   t = t1.toUpperCase();
// Extract first/next text character and match with alpha Array...
   for(var i = 0; i < t.length; i++) {
      ch = t.substring(i, i+1);
      for(var j = 0; j < alpha.length; j++) {
         if(ch == alpha[j]) {
            m = m + cw[j] + " ";
            break;
         }
      }
   }
   document.form.cw.value = m;
}
function cw2alpha() {
   t = ""; s = 0; idx = 1;
// Read CW window...
   m = document.form.cw.value;
   m = m + " ";
   len = m.length - 1;
        while((idx != -1) && (idx < len)) {
// Find first/next space and extract next CW character...
     idx = m.indexOf(" ", s);
      if(idx != -1) {
         m1 = m.substring(s, idx);
// Match CW character with CW Array and copy Alpha to text...
         for(var j = 0; j < cw.length; j++) {
            if(m1 == cw[j]) {
               t = t + alpha[j];
               break;
            }
         }
      }
// Add an extra space between words...
      if(m.substring(idx+1, idx+2) == " ") {
         t = t + " ";
         idx++;
       }
// If CW character is not matched, move pointer and try again...
// (Helps with unpredictable characters introduced during cutting
//  and pasting into the CW window)...
      if((j == cw.length) &&
          (m.substring(s,idx+1) != " ") &&
         (idx < len)) {
         idx = idx - m1.length;
      }
      idx++;
      s = idx;
   }
// Write text to text window...
   document.form.text.value = t;
}
function ccw() {
   document.form.cw.value = "";
} 
function ct() {
  document.form.text.value = "";
}

</script> 
</head>
<body bgcolor="#bfefff" background="../images/image2.jpg"   OnLoad="setup()">
<center><h3>Конвертер телеграфного коду (коду Морзе ;)</h3></center>
<form name="form" action=""><b>Наберіть або вставьте текст у це вікно</b><br>
 <input type="button" value="Convert to CW" OnClick="alpha2cw()">
<input type="button" value="Clear Window" OnClick="ct()">
<textarea rows="6" cols="50" name="text" wrap="virtual"></textarea>
<p><b>Наберіть або вставьте телеграфний код (CW) у це вікно</b><br>
 <input type="button" value="Convert to Text" OnClick="cw2alpha()">
<input type="button" value="Clear Window" OnClick="ccw()">
<textarea rows="6" cols="50" name="cw" wrap="virtual"></textarea>
</center> -->

</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
