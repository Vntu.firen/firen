<?php
	$title = "Студенти ФІРЕН відвідали SAMSUNG";
?>

              <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>
<b>Студенти Вінницького національного технічного університету з Факультету інфокомунікацій, радіоелектроніки та наносистем відвідали  освітній центр Samsung Academy в місті Києві. </b><br><br>
<a href="img/samsung/firen_samsung_00.jpg" target=_blank><IMG src="img/samsung/firen_samsung_00.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br> 
В освітньому центры Samsung Academy було проведено ознайомлення з історією розвитку компанії та її внутрішньою структурою.
<br><br>
<a href="img/samsung/firen_samsung_01.jpg" target=_blank><IMG src="img/samsung/firen_samsung_01.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_02.jpg" target=_blank><IMG src="img/samsung/firen_samsung_02.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_03.jpg" target=_blank><IMG src="img/samsung/firen_samsung_03.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_04.jpg" target=_blank><IMG src="img/samsung/firen_samsung_04.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_05.jpg" target=_blank><IMG src="img/samsung/firen_samsung_05.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_06.jpg" target=_blank><IMG src="img/samsung/firen_samsung_06.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
Окрім цього кожен студент мав змогу відвідати насичену і неймовірно цікаву лекцію за одним з напрямків електроніки (мобільні пристрої, аудіо/відео та побутова техніка).
<br><br>
<a href="img/samsung/firen_samsung_08.jpg" target=_blank><IMG src="img/samsung/firen_samsung_08.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_10.jpg" target=_blank><IMG src="img/samsung/firen_samsung_10.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_11.jpg" target=_blank><IMG src="img/samsung/firen_samsung_11.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_12.jpg" target=_blank><IMG src="img/samsung/firen_samsung_12.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
Для студентів технічного вишу це була чудова можливість поспілкуватися з фахівцями, отримати інформацію про найсучасніші тенденції розвитку ринку електроніки, ознайомитись з новітніми технологіями і методами виробництва сучасної електроніки, здобути вичерпні ґрунтовні рекомендації щодо вбору для себе сучасних девайсів. За результатами роботи провели тестування студентів, за допомогою якого кожен мав змогу перевірити здобуті знання.
<br><br>
<a href="img/samsung/firen_samsung_13.jpg" target=_blank><IMG src="img/samsung/firen_samsung_13.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_14.jpg" target=_blank><IMG src="img/samsung/firen_samsung_14.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_15.jpg" target=_blank><IMG src="img/samsung/firen_samsung_15.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_16.jpg" target=_blank><IMG src="img/samsung/firen_samsung_16.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_17.jpg" target=_blank><IMG src="img/samsung/firen_samsung_17.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_18.jpg" target=_blank><IMG src="img/samsung/firen_samsung_18.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
З маленької торгівельної компанії Samsung перетворилася на корпорацію світового класу, бізнес якої охоплює прогресивні технології, виробництво напівпровідників, будівництво хмарочосів і заводів, нафтохімію, фінанси та багато іншого. Відкриття, винаходи та інноваційні продукти дали змогу компанії стати лідером цих індустрій, постійно розвиваючи їх.
<br><br>
<a href="img/samsung/firen_samsung_19.jpg" target=_blank><IMG src="img/samsung/firen_samsung_19.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_20.jpg" target=_blank><IMG src="img/samsung/firen_samsung_20.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_21.jpg" target=_blank><IMG src="img/samsung/firen_samsung_21.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_22.jpg" target=_blank><IMG src="img/samsung/firen_samsung_22.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_23.jpg" target=_blank><IMG src="img/samsung/firen_samsung_23.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_24.jpg" target=_blank><IMG src="img/samsung/firen_samsung_24.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
Компанія Samsung незмінно ділиться з людьми по всьому світу своїм досвідом, від інновацій у сфері споживчої електроніки до біофармацевтичних розробок. Крім того, компанія Samsung вкладає інвестиції в навчання та розвиток своїх співробітників, і це є основою успіху компанії.
<br><br>
<a href="img/samsung/firen_samsung_25.jpg" target=_blank><IMG src="img/samsung/firen_samsung_25.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>
<a href="img/samsung/firen_samsung_26.jpg" target=_blank><IMG src="img/samsung/firen_samsung_26.jpg" width=500 alt="Екскурсія студентів в Samsung" title="Екскурсія студентів ФІРЕН в Samsung" border=0></a><br><br>

Мета компанії полягає в тому, щоб залучити до співпраці яскравих особистостей з інноваційним баченням, що є одним з необхідних чинників для утримання лідерства в цифрову епоху. 
</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
