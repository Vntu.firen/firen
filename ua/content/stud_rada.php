<?php
	$title = "СТУДЕНТСЬКА РАДА ФІРЕН";
?>

              <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>

<I>Голова СтудРади ФІРЕН</I> <b>Мельник Аліна Володимирівна</b><br><br>
<IMG src="img/stud/melnyk.jpg" width=200 border=0><br><br>
Про себе:  Народилась 27 лютого 1998 року у м. Вінниця. Випускниця Вінницького технічного ліцею,  де брала активну участь у творчих заходах та концертах. Після вступу у Вінницький національний технічний університет, одразу потрапила до активу студентського самоврядування, у 2015-2016 н.р. обіймала посаду секретаря СР. У 2016-2017 н.р. обіймала посаду голови СР. Староста БІО-15б. Була організатором студентської осені.<br>
Вважаю,  що кожному необхідно займати активну позицію та проявляти себе заради процвітання факультету.
<br><br>
<I>Заступник голови СтудРади ФІРЕН</I> <b>Перевознюк Сергій Сергійович</b><br><br>
<IMG src="img/stud/perevoznyk.jpg" width=200 border=0><br><br>
Про себе: Народився 5 березня 1999 року у місті Вінниця. Навчався у школі №20, де активно брав участь в усіх заходах та в їх організації. Займався багатьма видами спорту. Цікавлюсь юридичною діяльністю, знаю закони. Зараз я студент 2 курсу ФІРЕН, староста групи ТК-16б. З перших днів навчання приймав участь в усіх заходах. Активний, комунікабельний і готовий працювати. <br>
Хочу зробити свій внесок у розвиток факультету та ВНТУ, маю безліч ідей та хочу втілити їх.
<br><br>
<I>Голова інформаційно-аналітчного департаменту СтудРади ФІРЕН</I><br> <b>Шевчук В’ячеслав Валерійович</b><br><br>
<IMG src="img/stud/shevchuk.jpg" width=200 border=0><br><br>
Про себе: Ще зі школи проявляв активність в організаційних заходах та їх проведенні. Успішно брав участь у різних спортивних змаганнях. Студент  3 курсу групи ТКП-15б. З першого курсу був в активі іформаційно-аналітичного департаменту СР та профоргом групи.  У 2016-2017 н.р. обіймав посаду голови цього ж департаменту. Ініціативний та наполегливий, маю ряд ідей, які планую виконати в цьому році. 
<br><br>
<I>Голова спортивно-оздоровчого департаменту СтудРади ФІРЕН</I><br> <b>Мельничук Ольга Іванівна</b><br><br>
<IMG src="img/stud/melnychuk.jpg" width=200 border=0><br><br>
Про себе: народилася 20 липня в с. Ступник  Хмільницького району Вінницької області. Студентка групи  ТКП-14б. Активна, наполеглива та відповідальна. <br>
КМС України з легкої атлетики, член збірної команди області. Чемпіонка Універсіади області, обласного кросу та срібна призерка INTERSPORT UKRAINE RUN 2016. Чемпіонка України з естафетного бігу - рішення ВК від 17.08.2017 року √1901 м. Вінниця.
<br><br><br><br>

Вас вітає колектив Студентської  ради факультету інфокомунікацій, радіоелектроніки та наносистем! <br><br>
Цього навчального року в нас сформувався колектив з людей, які мають досвід роботи у студентській  раді або в напрямку свого департаменту. Це полегшує роботу та дає можливість показати хороший приклад Нашим студентам. Завдяки  взаєморозумінні та тривалій сумісній роботі сформували доволі сильну команду.<br><br>
З великою радістю приймемо в свою команду активних студентів факультету. Пропонуємо кожному зробити свій внесок  у розвиток факультету :) <br>
<br>
&nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/firen_studrada.pdf' target=_blank><img src=img/icons/pdf.jpg title='формат PDF'></a> <br><br><br><br>


<!--<I>Голова СтудРади ІнРТЗП</I> <b>Микола Кобися</b><br><br>

<IMG src="img/stud/kobusya.jpg" width=200 border=0><br><br>
<I>http://vk.com/id67803444</I><br>
Група: РА-13б <br>
Тел.:  0931501881; 0976481528<br>
<br><br><br> -->
<u><b>Склад СтудРади ФІРЕН (ФРТЗП, ІнРТЗП) по роках</b></u><br><br>

<b>2004</b> – голова студентської ради  <b>Коваль Костянтин</b><br><br>

<b>2006</b> – головою студентської ради ІнРТЗП обирається <b>Бєлов Володимир</b><br>
<I>Склад: Пономарьов Віктор (заст.голови), Пилинь Ірина (секретар), Рудий Сергій, Притула Максим, Новак Олексій, Мартинюк Богдан, Пастух Олександр</I>
<br><br>

<b>2008</b> – голова студентської ради - <b>Мартинюк Богдан</b><br><br>

<b>До 2011</b> головою студентської ради факультету РТТК була <b>Артемчук Ольга</b>, головою студради фМБЕП – <b>Новицька Іра</b>.<br><br>

<b>2011 р.</b> - головою студентської ради факультету РТТК був <b>Іваськевич Мирослав</b>, головою студради фМБЕП – <b>Філіппов Ігор</b>.<br><br>

<b>2012 р.- березень 2013р.</b> головою Ін РТЗП став <b>Іваськевич Мирослав</b>.<br><br>

<b>березень 2013-листопад 2013</b> головою Ін РТЗП була <b>Віт Ольга</b>.<br><br>

<b>Листопад 2013</b> став головою <b>Микола Кобися</b>.<br><br>
<IMG src="img/stud/kobusya.jpg" width=200 border=0><br><br>

<center>
<br><br><u><b>Студентська Рада ФРТЗП</b></u><br><br>

<a href=img/stud/sostav/1.jpg target=_blank><IMG src="img/stud/sostav/1.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/2.jpg target=_blank><IMG src="img/stud/sostav/2.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/3.jpg target=_blank><IMG src="img/stud/sostav/3.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/4.jpg target=_blank><IMG src="img/stud/sostav/4.jpg" width=100 title='Студентська рада ФРТЗП' border=0>
<br><br>
<a href=img/stud/sostav/5.jpg target=_blank><IMG src="img/stud/sostav/5.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/6.jpg target=_blank><IMG src="img/stud/sostav/6.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/7.jpg target=_blank><IMG src="img/stud/sostav/7.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/8.jpg target=_blank><IMG src="img/stud/sostav/8.jpg" width=100 title='Студентська рада ФРТЗП' border=0>
<br><br>
<a href=img/stud/sostav/9.jpg target=_blank><IMG src="img/stud/sostav/9.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/10.jpg target=_blank><IMG src="img/stud/sostav/10.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/11.jpg target=_blank><IMG src="img/stud/sostav/11.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/12.jpg target=_blank><IMG src="img/stud/sostav/12.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a>
<br><br>
<a href=img/stud/sostav/13.jpg target=_blank><IMG src="img/stud/sostav/13.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/14.jpg target=_blank><IMG src="img/stud/sostav/14.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/15.jpg target=_blank><IMG src="img/stud/sostav/15.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/16.jpg target=_blank><IMG src="img/stud/sostav/16.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a>
<br><br>
<a href=img/stud/sostav/17.jpg target=_blank><IMG src="img/stud/sostav/17.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/18.jpg target=_blank><IMG src="img/stud/sostav/18.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/19.jpg target=_blank><IMG src="img/stud/sostav/19.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a> &nbsp;&nbsp;&nbsp;
<a href=img/stud/sostav/20.jpg target=_blank><IMG src="img/stud/sostav/20.jpg" width=100 title='Студентська рада ФРТЗП' border=0></a>
<br><br>
</center>

<!--
<IMG src="img/stud/sr1.jpg" width=500 border=0><br><br>

<IMG src="img/stud/sr2.jpg" width=500 border=0><br><br>
-->

</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
