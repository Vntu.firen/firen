<?php
	$title = "РОЗКЛАД ЗАНЯТЬ ПО ФІРЕН";
?>



         <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>


&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад занять:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 1 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=2' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 3 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=3' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 4 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=4' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 5 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=5' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

<br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Додатковий розклад:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;1 курс<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTXkbbzHJ3XFjmDBtng4xJHyh4Vls3qFpp3yCUskzCJZylTNNdJPXl7zIw3ujb2CGJRNIdh64JGjyS6/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;2 курс<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRodYKvhWfFADtf_Jpnd0XY9DGou0UoKf7RcoMauKNerWxtHb7Lqif5kP56UzVR5PgZ-XEQChOVO28g/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;3 курс<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vQ_r-q1ybSuFKGLKyg5MCb4iCC3n6mFfws02h676sbveYqpoGw6E2o0Xq2OeEpp6fM7Q5kZ9nyz-MuC/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;4 курс<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTBz8lSkZx3oqhXrc0zZeZn3KYygf0PTMWrdmLpVu8it5Woxyskw3kl_eUvnB5rnvQDFHWkJHLfuBEG/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;5 курс<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTyCQnoYRDexnMqPIE9TVAWS_jaIleLRsFt0hfUQeVPSP4XFnYYFWn4VhCINhKXl4cl4d9doBd28NEQ/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>


<br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад занять заочне відділення:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 1 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=1&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=2&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 3 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=3&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 4 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=4&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 5 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=5&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

<br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії 1 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=1&exam=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії 2 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=2&exam=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії 3 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=3&exam=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії 4 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=4&exam=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії 5 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=5&exam=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>



<br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії заочне відділення:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії заочн. 1 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=1&exam=1&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії заочн. 2 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=2&exam=1&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії заочн. 3 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=3&exam=1&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії заочн. 4 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=4&exam=1&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесії заочн. 5 курс ФІРЕН<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='http://iq.vntu.edu.ua/b04213/curriculum/c_sched_course.php?dep_id=211&course=5&exam=1&z=1' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>



<!--<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTzCPXYU4fufbwlzIC1N1o2hGKfdSJ2OJpTaXnUb50GwkjYMD2uoc6zjgO1VM9FZBfKAqbv9lFy8EPi/pubhtml?gid=634599945&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
-->

<!--
<br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 1 курс ФІРЕН на 2 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vS1b6qGksRMejHXLc7zRlVgmNGlCoZV-9eTBi3bHoloxQtsG6wJTkEqYYR-MDKXxZEYLBfOyOFbnzvS/pubhtml?gid=222319815&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
<!-- &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2 курс ФІРЕН на 2 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vQKERL8nuumIbg7FOYSbS076KYfqvSCkAsYjedgrCVj1k7NNAfyUhf3fDc_fBD1qd1ZXT8oon3_yKza/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 3 курс ФІРЕН на 2 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vQhAy-2s0GAqe81NP4ifmhqjxRUXZv6Issi41ZCD03XPRHix3tXS20RANA0UYNNRbL03vIBDTv-I8RZ/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 4 курс ФІРЕН на 2 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vS1awc46wDjpCAUmjz0r_Djxrr6f-eKo07NR-RQPla_MYf47YaI_HCbzJS1AauG2VfkeoVU_PdQacnj/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
-->
<!-- &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 1 курс магістри ФІРЕН на 2 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTHJwScV1FPZZpQgqOs3aPHgVFm4wMeqPD2UDKO9juEU08UZrBCBWz2FaTHLVsuWlXHf29-7OmcEEU2/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>


&nbsp;&nbsp;&nbsp;&nbsp;* - Додатковий розклад занять 3 та 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTV87nWpnlkt3rozMxhx6XlNq605nq0MZk5OL_JfoIZ6mGatdmHnUJZudEANtmxc4QaEIJNIuFqQd-0/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
<!--
&nbsp;&nbsp;&nbsp;&nbsp;* - Додатковий розклад занять 4 та 2 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRhQbPqr5B4FlcrXXyg-Ahh52CCI9Gm2qmBdR1q2e6IQQNvWo4vhcyZGHwUQWW01a7_a0pYGC6POKML/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
-->

<!-- <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять магістри заочної форми:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vQwVyQkPqZikNBZ5BmqWNk_ajVOTcQDlXjHyt7PTfzOcf8GTzd6v3TtLUZD6-JYQ1B6cyiGkznvmNGm/pubhtml?gid=221318224&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2, 4 курс заочної форми:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTjnFasLCxH6Ie324pdxYKwDTAzg6zf4VStCl6V8XDZTfTX9IgxqUpqhp5iwF5QHgg0cTLQWVarG8di/pubhtml?gid=241836290&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b><br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/document/d/e/2PACX-1vQ8JBPRpKNVRN9iaAuwHWMTuq7ojefXvNzF0XJsVD-lA7Ejx6e4EQiG90I0_W5_FgBsLQDolFcaj_ww/pub' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 2 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vT6HFBezt7RVpd3NNLSKmoYUHjKOgcbw8xzZFFj4kZTCX_8cTD6GJq9EEL058byqEeGgdog1JBRuCki/pubhtml?gid=1402252835&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї магістри 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTnLM6qXP1qvmD-5SL8cbLuqOsroBP6bfsd6jkZ27pRJYRmcOgrtPUrzHzL-rsZjZS1v3t6EJ0TDKq9/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 




&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3, 1 курс заочної форми навчання:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTJ8kt58e3Ssdd93yizWX-c7u7rLO2KZna3MXyH-vBCF7c-1wR4fwHQ2k0965oz-o_Hl_DdO6rMWFay/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 4, 2 курс заочної форми навчання:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vQW1c_AB4aVjAOM7YO-ulKgPH-2eNKPInRbpqGi6Hc9qMR6ZLyGI0QkOL2Fp-0cPofXu6ioLU2DwUfY/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 


<!--
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3, 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRzmqeyUnkcqMWSbFy6Kn8BSnYxw8T1MnwcWARYOnl_J7uLeQhdRN29c2Yn28IFARqfc4bcPXi2CB0A/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 4, 2 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRL-2YVhwmIuTpQwkkRimdcjJXj2Xf-qrShqnikXsfoJjA1eRbGMwcpZSiB7yLzD3bu2RPPe8VLzPz2/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 
-->

<!--
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 2, 4 курс заочної форми:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vREyvwWg1lQksd6diTv0btx9I3vz210Ed0yXpbPnEWsSrDTtqukISszM28mp6fwOPT4lxNyyCpk32nP/pubhtml?gid=186623562&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> 

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 курс (магістри) заочної форми:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vQzb0CZ_D8YIDzkMtr6FR9wtJ_Vrz0BrV85nyXIFYOp1ttBWBpQ4L3l4GQFUz9Wq4VpmeKsqfToS51O/pubhtml?gid=1195008314&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> <br><br> 

-->



<br><br>



<!-- &nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 м курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRnVgHL0tR6ZLIfOD1p2E1K_Bg8dUpoe99ZkzUr8KO1_N7ecVEk_hJpOhaZRsoAHgG8Vf8wfgUama1n/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 4 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vSQB0fgal06UakmiPF9eunndTf9YaBqT0Y1uVG5CaRjO6QBtn8LH91euWoorUtnvtv9byKyjR8-z2DS/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vSof87f1-6ePo2c--tWRMZV3MTuhCJdUwJi0zM3EH7mHbGix-70jHVLaDjVis2-az8v-Ue19rjETDyY/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 2 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vQna5Ea0zdRf4YaHiaXELkMere6HeTkp7htL8jsGdbhqzMaLETvhnNlpfpcde-JDQBTEoWciX8hMmJt/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/document/d/e/2PACX-1vSiI-EI7vH406V_siRATvi8WpAYNjcrd9H9UwBiMmG3rn0WDjYmN_sz6WdYgviXzEkQAt0lx7Kmw1xs/pub' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> <br><br> 
<br><br> -->


<!-- &nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5, 3 курс заочної форми:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vREyt-_GgvUFB4OZD-a6p19cv1XBPbSGRMVFxnrcwtvARvpK5BdIDySvNFG_cK5H-0aa2eH_nL5EY1D/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> <br><br> 
<br><br> <br><br> 
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад занять:</b>
<br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 1 курс ФІРЕН на 1 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRd8UHrJ5yOgVM1hGZmN_K7FSPZhqd_4t9Gw3TTjqxmHDapgMShZUcFXaarwPrMwMTDmJLtN8ae1OjD/pubhtml?gid=1681040910&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2 курс ФІРЕН на 1 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRd8UHrJ5yOgVM1hGZmN_K7FSPZhqd_4t9Gw3TTjqxmHDapgMShZUcFXaarwPrMwMTDmJLtN8ae1OjD/pubhtml?gid=799441251&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 3 курс ФІРЕН на 1 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRd8UHrJ5yOgVM1hGZmN_K7FSPZhqd_4t9Gw3TTjqxmHDapgMShZUcFXaarwPrMwMTDmJLtN8ae1OjD/pubhtml?gid=66274212&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 4 курс ФІРЕН на 1 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRd8UHrJ5yOgVM1hGZmN_K7FSPZhqd_4t9Gw3TTjqxmHDapgMShZUcFXaarwPrMwMTDmJLtN8ae1OjD/pubhtml?gid=381996961&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>

-->

<!--  &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 5сп курс ФІРЕН на 2 семестр 2016/2017 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1pkIgZYyzevxzL39bySvnQCbQGVCeNhbWbvbsEYBPmPE/pubhtml?gid=1373671929&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> -->
<!--  &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 5м курс ФІРЕН на 1 семестр 2017/2018 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; 
<a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vRd8UHrJ5yOgVM1hGZmN_K7FSPZhqd_4t9Gw3TTjqxmHDapgMShZUcFXaarwPrMwMTDmJLtN8ae1OjD/pubhtml?gid=733443643&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
-->
<!--
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 м курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/document/d/1MaMvZWgX1ksDYuyeQqUKlgot6hC7_Y3P4crKb9MLwTU/pub' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> <br><br> 

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 сп курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/document/d/1SazCeJDLqCNqEoMaJW_7VDhTWgtQ22ea0iiL6m1tCJk/pub' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> <br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 4 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/document/d/1Tv_ncBL14gGyGJarRxor4bX4CJFdSzckYxlzPyNkWok/pub' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> <br><br>

<!-- &nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1pdTpKwi82mcgIq1SQfQEmzchhecFYOo6yNIdw8-MDDU/pubhtml' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 2 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/document/d/1oVzCa2_InidgGAEyMohHnAjWlHFgURkbBWmCTnt7MUE/pub' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> <br><br> -->

<!--
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/document/d/1QxIy_W8kk6nQ0fxipPMeaXMCiX91RkX9rMw-bwpn7_E/pub' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> <br><br> 
-->

<!-- &nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад занять:</b>
<br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 1 курс ФІРЕН на 1 семестр 2016/2017 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1-3WwIzv3cV17nNsZg0aPoSfKAA75q7ecB-R6oHo4JIA/pubhtml?gid=477990628&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2 курс ФІРЕН на 1 семестр 2016/2017 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1-3WwIzv3cV17nNsZg0aPoSfKAA75q7ecB-R6oHo4JIA/pubhtml?gid=1254512011&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 3 курс ФІРЕН на 1 семестр 2016/2017 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1-3WwIzv3cV17nNsZg0aPoSfKAA75q7ecB-R6oHo4JIA/pubhtml?gid=1459856012&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 4 курс ФІРЕН на 1 семестр 2016/2017 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1-3WwIzv3cV17nNsZg0aPoSfKAA75q7ecB-R6oHo4JIA/pubhtml?gid=436983751&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 5сп курс ФІРЕН на 1 семестр 2016/2017 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1-3WwIzv3cV17nNsZg0aPoSfKAA75q7ecB-R6oHo4JIA/pubhtml?gid=1712961360&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 5м курс ФІРЕН на 1 семестр 2016/2017 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='https://docs.google.com/spreadsheets/d/1-3WwIzv3cV17nNsZg0aPoSfKAA75q7ecB-R6oHo4JIA/pubhtml?gid=1918409334&single=true' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br>
-->

<!-- 
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять ФРТЗП на 3 семестр 2015/2016 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/rozklad_mag_3_2016.xls' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> (магістри 5-й курс) <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/2016_3sem.xls' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> (3 курс МС) <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/2016_rp.xls' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> (робітнича професія) <br><br>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять ФРТЗП на 2 семестр 2015/2016 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/2015-16_2sem.xls' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> (5-й курс) <br><br>
&nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/2015_16_2sem1-4.xls' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> (1-4-й курс) <br><br>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 курс магістри:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/mag_2_2016.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a>  <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 курс магістри на базі спеціаліста:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/mag_sp_2_2016.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a>  <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 курс спеціалісти:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/sp_2_2016.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a>  <br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2016-3.xls' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a>  <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 2 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2016-2.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a>  <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2016-1.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a>  <br><br>
--> 
<!--
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 курс магістри:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-5m.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a>  <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 5 курс спеціалісти:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-5.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a>  <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 4 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-4.docx' target=_blank><img src=img/icons/docx.jpg title='формат DOCx'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-3.exl' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-3.pdf' target=_blank><img src=img/icons/pdf.jpg title='формат PDF'></a>   <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 2 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-2.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-2.pdf' target=_blank><img src=img/icons/pdf.jpg title='формат PDF'></a> <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 1 курс:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-1.doc' target=_blank><img src=img/icons/doc.jpg title='формат DOC'></a> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/ses2015-1.pdf' target=_blank><img src=img/icons/pdf.jpg title='формат PDF'></a> <br><br>
-->
<!--
<br><br><br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад занять:</b>
<br><br>

 &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять ФРТЗП на 1 семестр 2015/2016 н.р.:<br><br> &nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/2015-2016.xls' target=_blank><img src=img/icons/exl.jpg title='формат EXL'></a> <br><br> -->

<!-- &nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2-й семестр: <a href='../files/frtzp_2_2014-15.xls' target=_blank>завантажити</a> (формат XLS)<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 3-й семестр магістри: <a href='../files/5k_mag_3.xls' target=_blank>завантажити</a> (формат XLS)<br><br>
<br>
<i>&nbsp;&nbsp;&nbsp;&nbsp; Додатковий розклад занять:<br><br>
&nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/2015dop.xls' target=_blank>Завантажити додатковий розклад</a> (формат XLS) </i>
<br><br>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї Магістри: <a href='../files/2014-15_ses_mag.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї Спеціалісти: <a href='../files/2014-15_ses_spec.doc ' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 4 курс: <a href='../files/2014-15_ses4k2.doc' target=_blank>завантажити</a> (формат DOC)<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3 курс: <a href='../files/ses_3k_2_2014-15.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 3 курс (МС): <a href='../files/ses_3k_2_mc_2014-15.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 2 курс: <a href='../files/ses_2k_2_2014-15.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сесiї 1 курс: <a href='../files/ses_1k_2_2014-15.doc' target=_blank>завантажити</a> (формат DOC)<br><br>





<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї Магістри: <a href='../files/2014-15_sesmag.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 5 курс денне: <a href='../files/2014-15_ses5k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 4 курс денне: <a href='../files/2014-15_ses4k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 3 курс денне: <a href='../files/2014-15_ses3k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 2 курс денне: <a href='../files/2014-15_ses2k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 1 курс денне: <a href='../files/2014-15_ses1k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
-->                                                                       

<!--
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Робітнича професія:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад: <a href='../files/rp_2014.xls' target=_blank>завантажити</a> (формат XLS)<br><br>
<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад сесії:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї Магістри: <a href='../files/2014-15sesmag.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 5 курс денне: <a href='../files/2014-15ses5k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 4 курс денне: <a href='../files/2014-15ses4k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 3 курс денне: <a href='../files/2014-15ses3k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 2 курс денне: <a href='../files/2014-15ses2k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 1 курс денне: <a href='../files/2014-15ses1k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Розклад занять:</b>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 2 семестр: <a href='../files/inrtzp_2_2013_14.xls' target=_blank>завантажити</a> (формат XLS)<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять 1 семестр: <a href='../files/inrtzp_1_2013_14.xls' target=_blank>завантажити</a> (формат XLS)<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять магістри 1 семестр: <a href='../files/inrtzp_mag_1_2013_14.xls' target=_blank>завантажити</a> (формат XLS)<br><br><br><br>



&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 5 курс (мс): <a href='../files/2013-14_5k_mc.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 5 курс: <a href='../files/2013-14_5k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 4 курс: <a href='../files/2013-14_4k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 3 курс: <a href='../files/2013-14_3k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 2 курс: <a href='../files/2013-14_2k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 1 курс: <a href='../files/2013-14_1k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>


&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять IНРТЗП: <a href='../files/inrtzp_2012_13.xls' target=_blank>завантажити</a> (формат XLS) <br><br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Робоча професія 1 курс: <a href='../files/1k.xls' target=_blank>завантажити</a> (формат XLS) <br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Робоча професія 2 курс: <a href='../files/2k.xls' target=_blank>завантажити</a> (формат XLS) <br><br><br><br>


&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 4 курс денне: <a href='../files/session4k.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 3 курс денне: <a href='../files/2012-13_3k_2.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 2 курс денне: <a href='../files/2012-13_2k_2.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Розклад сессiї 1 курс денне: <a href='../files/2012-13_1k_2.doc' target=_blank>завантажити</a> (формат DOC)<br><br>
-->

<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;Телефон директорату: <b> +38(0432)46-66-65</b> <!-- +38(0432)59-82-69 -->
<br><br><br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Збережіть адресу розкладу!<br><br>
&nbsp;&nbsp;&nbsp;&nbsp;<IMG src="img/rasp.png" border=0><br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Використання:<br>
&nbsp;&nbsp;&nbsp;&nbsp;1. Візьміть мобільний телефон з камерою<br>
&nbsp;&nbsp;&nbsp;&nbsp;2. Запустіть програму для сканування коду<br>
&nbsp;&nbsp;&nbsp;&nbsp;3. Спрямуйте об'єктив камери на код<br>
&nbsp;&nbsp;&nbsp;&nbsp;4. Отримайте інформацію!<br>


<!--
&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять факультету РТТК 2 семестр: <a href='../files/frttk_2_2011_12.xls' target=_blank>завантажити</a> (формат XLS)<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять факультету МБЕП 2 семестр: <a href='../files/fmbep_2_2011_12.xls' target=_blank>завантажити</a> (формат XLS) <br><br><br>

<hr><br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять факультету РТТК 1 семестр: <a href='../files/frttk_1_2011_12.xls' target=_blank>завантажити</a> (формат XLS)<br><br>

&nbsp;&nbsp;&nbsp;&nbsp;Розклад занять факультету МБЕП 1 семестр: <a href='../files/fmbep_1_2011_12.xls' target=_blank>завантажити</a> (формат XLS) <br><br>
<br><br>
&nbsp;&nbsp;&nbsp;&nbsp; Додатковий розклад занять:<br><br>
&nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/dopolnenie.zip' target=_blank>Завантажити додатковий розклад</a> (формат ZIP) <br><br>

<br><br>
&nbsp;&nbsp;&nbsp;&nbsp; Розклад заліково-екзаменаційної сесії:<br><br>
&nbsp;&nbsp;&nbsp;&nbsp; <a href='../files/session.xls' target=_blank>Завантажити розклад сесії</a> (формат XLS) <br><br>
--!>



</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>

