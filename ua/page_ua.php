<HTML><TITLE>Факультет інфокомунікацій, радіоелектроніки та наносистем ФІРЕН ВНТУ - <?=$title?></TITLE>
<HEAD><META http-equiv=Content-Type content="text/html; charset=utf-8">

<meta property="og:image" content="http://inrtzp.vntu.edu.ua/ua/img/firen2.jpg" />

<LINK href="styles.css" type=text/css rel=stylesheet>
<LINK REL="shortcut icon" HREF="favicon.ico" TYPE="image/x-icon">
                        <STYLE>TD {
	FONT-SIZE: 11px; COLOR: #4f7182; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif
}

#menu{padding:0;margin:0}
#menu ul{padding:0;margin:0}

</STYLE>

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-46175107-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>


<script type="text/javascript">
function showElement(layer){
var myLayer = document.getElementById(layer);
if(myLayer.style.display=="none"){
 myLayer.style.display="block";
 myLayer.backgroundPosition="top";
 } else { 
 myLayer.style.display="none";
 }
}
</script>


<meta name="google-translate-customization" content="774e135109a90f24-1ceb17019a4a6fb2-g4085e09e9660397d-12"></meta>



</HEAD>

<BODY bgColor=#6ab4db background="img/a1.gif" topMargin=0 >
<TABLE cellSpacing=0 cellPadding=0 width=800 align=center border=0>
  <TBODY>
  <TR>
    <TD>
      <TABLE height=107 cellSpacing=0 cellPadding=0 width=800 bgColor=#6ab4db 
      border=0>
        <TBODY>
        <TR vAlign=center align=left>
          <TD align=left width=251 
          background="img/a1.gif" 
            height=10>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 

<!-- <font class=lang1>
<A class=lang href="../ru/"> рус</a> | <A class=lang href="../en/"> eng </a>| <A class=lang href="../ua/"> укр</a></font>
-->
<a href='http://vntu.edu.ua' target=_top><img src="img/vntu_bw.jpg" title='ВНТУ' width=80 border=0></a>
      


</TD>
          <TD vAlign=bottom height=10>
            <TABLE cellSpacing=0 cellPadding=0 width="100%" border=0>
              <TBODY>
              <TR>
                <TD align=right width=493 
                background="img/verh.jpg" 
                height=87>
<font class=logo>Факультет інфокомунікацій, радіоелектроніки та наносистем</font>
</TD></TR>
              <TR>
	                <TD>
                  </TD></TR></TBODY></TABLE></TD>
          <TD width=57 height=10><IMG height=87 
            alt="" src="img/verh2.jpg" 
        width=56></TD></TR>
        <TR>
          <TD align=right width=251 
          background="img/a1.gif" height=8>
            <TABLE height=8 cellSpacing=0 cellPadding=0 width=100 border=0>
              <TBODY>
              <TR bgColor=#ffffff>
                <TD height=8><IMG height=8 
                  src="img/ugol1.gif" 
                  width=21 border=0></TD>
                <TD height=8></TD></TR></TBODY></TABLE></TD>
          <TD align=right width=493 bgColor=#ffffff height=8>
            <TABLE height=8 cellSpacing=0 cellPadding=0 width=100 border=0>
              <TBODY>
              <TR bgColor=#ffffff>
                <TD width=75 height=8></TD>
                <TD width=25 height=8><IMG height=8 
                  src="img/ugol2.gif" 
                  width=28 border=0></TD></TR></TBODY></TABLE></TD>
          <TD width=57 height=8><IMG height=8 
            alt="" src="img/verh5.jpg" 
        width=56></TD></TR>
        <TR>
          <TD width=251 bgColor=#ffffff height=20><IMG height=20 
            src="img/ugol6.gif" width=188 
            border=0></TD>
          <TD align=right width=493 bgColor=#ffffff height=20>
            <TABLE height=20 cellSpacing=0 cellPadding=0 width=490 border=0>
              <TBODY>
              <TR>
                <TD class=srsm vAlign=center align=left width=482>
м.Вінниця, Хмельницьке шосе, 95, корпус 1
</TD><TD align=right width=7><IMG height=20 
                  src="img/ugol5.gif" width=8 
                  border=0></TD></TR></TBODY></TABLE></TD>
          <TD width=57 height=20><IMG height=20 
            alt="" src="img/verh6.gif" 
        width=56></TD></TR></TBODY></TABLE></TD></TR>
  <TR>
    <TD>
      <TABLE height=622 cellSpacing=0 cellPadding=0 width=800 border=0>
        <TBODY>
        <TR>
          <TD vAlign=top align=left width=188 
          background="img/fon3.gif" 
          bgColor=#69b3da>
            <TABLE cellSpacing=0 cellPadding=0 width=188 border=0>
              <TBODY>
              <TR>
                <TD vAlign=top align=right 
                background="img/fon2.jpg" 
                bgColor=#104294>
                  <TABLE class=pc188 cellSpacing=0 cellPadding=0 width=188 
                  border=0>
                    <TBODY>

                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?main">Головна</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?history">Історія факультету</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?raspisanie">Розклад занять</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>



<!--                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?faculty">Факультети</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>
-->

<!-- всплывашка 1--> 
 <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg"  
                      height=34>
<a href="#" class="meny" onclick="javascript:showElement('1menu')">
Кафедри
</a>
<ul id="1menu" class="v-menu" style="display:none;padding: 0px; margin:0;">
<table cellSpacing=0 cellPadding=0> 
<TR><TD height=20></TD></TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="http://inrtzp.vntu.edu.ua/tkstb" target=_blank>ТКСТБ</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="http://bmi.vntu.edu.ua/" target=_blank>БМІ</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="http://ke.vntu.edu.ua/" target=_blank>ЕНС</A></TD>
                      </TR>
		    
<!--  <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="http://pkta.vntu.edu.ua" target=_blank>ПКТА</A></TD>
                      </TR>
-->
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="http://rt.vntu.edu.ua/" target=_blank>РТ</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="http://kmz.vntu.edu.ua/" target=_blank>Мовознавства</A></TD>
                      </TR>
</table></ul>
</TD><TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

<!--конец всплывашки -->



<!-- всплывашка 2-->  <!--  <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg"  
                      height=34>
<a href="#" class="meny" onclick="javascript:showElement('2menu')">
Напрямки підготовки
</a>
<ul id="2menu" class="v-menu" style="display:none;padding: 0px; margin:0;">
<table cellSpacing=0 cellPadding=0> 
<TR><TD height=20></TD></TR>

		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny_mal 
                      href="<?=$host?>?speciality" target=_top>Спеціальності і спеціалізації</A></TD>
                      </TR>

		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny_mal 
                      href="<?=$host?>?6050901" target=_top>6.050901 - Радіотехніка</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny_mal 
                      href="<?=$host?>?6050902" target=_top>6.050902 - Радіоелектронні апарати</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny_mal 
                      href="<?=$host?>?6050903" target=_top>6.050903 - Телекомунікації</A></TD>
                      </TR>

                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny_mal 
                      href="<?=$host?>?6050801" target=_top>6.050801 - Мікро- та наноелектроніка</A></TD>
                      </TR>

                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny_mal 
                      href="<?=$host?>?6050802" target=_top>6.050802 - Електронні пристрої та системи</A></TD>
                      </TR>

</table></ul></TD><TD class=pc10 width=10 height=34>&nbsp;</TD></TR>
-->
<!--конец всплывашки -->


                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?ects">ECTS-інформ.пакети</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?mtb">Матеріально-техн. база</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


<!-- всплывашка 3-->    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg"  
                      height=34>
<a href="#" class="meny" onclick="javascript:showElement('3menu')">
Інформаційний розділ
</a>
<ul id="3menu" class="v-menu" style="display:none;padding: 0px; margin:0;">
<table cellSpacing=0 cellPadding=0> 
<TR><TD height=20></TD></TR>

		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?samsung" >- SAMSUNG</A></TD>
                      </TR>

		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?info_kultura_i_sport" >- культура і спорт</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?info_best" >- BEST</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?info_abiturientu" >- абітурієнту</A></TD>
                      </TR>

<!--		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?info_vupysknuky" >- випускнику</A></TD>
                      </TR>
-->

		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?laws" >- законодавство</A></TD>
                      </TR>


		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?rtzp45" >- ФРТЗП 45 років! </A></TD>
                      </TR>


		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?gaudeamus" >- Gaudeamus</A></TD>
                      </TR>

</table></ul></TD><TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

<!--конец всплывашки -->


<!-- всплывашка 4-->    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg"  
                      height=34>
<a href="#" class="meny" onclick="javascript:showElement('4menu')">
Студентське життя
</a>
<ul id="4menu" class="v-menu" style="display:none;padding: 0px; margin:0;">
<table cellSpacing=0 cellPadding=0> 
<TR><TD height=20></TD></TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?stud_rada">- Студентська Рада</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?stud_life">- Наші заходи</A></TD>
                      </TR>
		      <TR>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?stud_photo">- Фотоальбом</A></TD>
                      </TR>

                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                      href="<?=$host?>?radioengineer">- Радіо Інженер</A></TD>
                      </TR>

</table></ul></TD><TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

<!--конец всплывашки -->



                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?inform">Оголошення</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?administration">Адміністрація</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>




                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?reference">Корисні посилання</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


          <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?feedback">Зворотній зв'язок</A></TD>
                     <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="http://elearn.vntu.edu.ua" target=_blank>Дистанц. освіта</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

<!--                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="http://photo.vntu.edu.ua/" target=_blank>Фотогалерея</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

                    <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="http://vntu.edu.ua/index.php?option=com_easybookreloaded&view=easybookreloaded&Itemid=43&lang=uk" target=_blank>Гостьова сторінка</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

-->

<!--          <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="http://www.vntu.edu.ua/" target=_blank>Сайт ВНТУ</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>
-->
          <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?map">Мапа університету</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

	          <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?radioclub">Радіоклуб</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

	          <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="https://iq.vntu.edu.ua/" target=_blank>Сторінка JetIQ</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


<!--          <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34><IMG height=15 hspace=6 
                        src="img/f1.gif" 
                        width=16></TD>
                      <TD class=pc134 vAlign=baseline align=left width=134 
                      background="img/knop.jpg" 
                      height=34><A class=meny 
                        href="<?=$host?>?mail">ФРТЗП WebMail</A></TD>
                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>

-->

          <TR>
                      <TD class=pc44 vAlign=top align=right width=44 
                        height=34></TD>
                      <TD class=pc134 vAlign=baseline align=center width=134 
                      background="" 
                      height=34>

<A class=meny href="http://inrtzp.vntu.edu.ua/ua/?sprtp2017" title='СПРТП-2017' target=_blank>
<img src=img/sprtp.jpg width=88 border=0>
</A>
<br><br>
<A class=meny href="http://vsort.org.ua" title='ВСОРТ' target=_blank>
<img src=img/vsortvntu.jpg width=88 border=0>
</A>
<br><br>
<A class=meny href="http://rt.vntu.edu.ua" title='Кафедра РТ' target=_blank>
<img src=img/rt.jpg width=88 border=0>
</A>
<br><br>
<A class=meny href="http://inrtzp.vntu.edu.ua/ua/?radioengineer" title='Студентське радіо Інженер' target=_blank>
<img src=img/ri.gif width=88 border=0>
</A>
<br><br>
<A class=meny href="http://inrtzp.vntu.edu.ua/ua/?pract" title='Розрахункова СБШД' target=_top>
<img src=img/sbshd.jpg border=0>
</A>
<br><br>
<A class=meny href="https://flightaware.com/adsb/stats/user/inrtzp" title='IoT ADS-B' target=_blank>
<img src=img/firen_lab.gif border=0>
</A>
<br><br>
<A class=meny href="http://inrtzp.vntu.edu.ua/ua/?dbmw" title='Онлайн калькулятор' target=_top>
<img src=img/dbmw.gif border=0>
</A>



</TD>

                      <TD class=pc10 width=10 height=34>&nbsp;</TD></TR>


</TBODY></TABLE></TD></TR>
              <TR>
                <TD vAlign=top align=left 
                background="img/fon2.jpg" 
                bgColor=#104294>
                  <TABLE height=5 cellSpacing=0 cellPadding=0 width=10 
                    border=0><TBODY>
                    <TR>
                      <TD></TD></TR></TBODY></TABLE>

                  <TABLE cellSpacing=0 cellPadding=0 width=188 border=0>
                    <TBODY>
                    
                    <TR>
                      <TD><IMG 
                        height=20 
                        src="img/ugol8.gif" 
                        width=188 border=0></A></TD></TR></TBODY></TABLE></TD></TR>



              <TR>
                <TD vAlign=top align=right>
                  <TABLE cellSpacing=0 cellPadding=0 border=0>
                    <TBODY>
                    <TR>
                      <TD vAlign=top align=right height=36>
</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD>
          <TD vAlign=top align=left width=555 bgColor=#ffffff>
            <TABLE cellSpacing=0 cellPadding=0 width=555 border=0>
              <TBODY>
              <TR>
                <TD>
                  <TABLE height=36 cellSpacing=0 cellPadding=0 width=494 
                  border=0>
                    <TBODY>
                    <TR>
                      <TD vAlign=center align=right width=319 
                      background="img/fon6.jpg" 
                      height=36>
                        <TABLE class=zaglstran cellSpacing=0 cellPadding=0 
                        width=299 border=0>
                          <TBODY>
                          <TR>
                            <TD vAlign=bottom align=right 
width=268>

		<?=$title?>


</TD>
                            <TD align=right width=31></TD></TR></TBODY></TABLE></TD>
                      <TD width=15 height=36><IMG height=36 
                        src="img/ugol14.gif" 
                        width=15></TD>
                      <TD vAlign=top align=right width=160 height=36>
                        </TD></TR></TBODY></TABLE></TD></TR>

            <TABLE cellSpacing=0 cellPadding=0 width=555 border=0>
              <TBODY>
              <TR>
                <TD vAlign=top align=left width=538>
                  <TABLE class=pc0 cellSpacing=6 cellPadding=0 width="100%" 
                  border=0>
                    <TBODY>
                    <TR>




                      <TD vAlign=top align=middle>



                        <TABLE class=pc500 id=table25 height=63 cellSpacing=1 
                        cellPadding=0 width=510 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-WEIGHT: normal; FONT-SIZE: 22px; COLOR: #104294; FONT-FAMILY: Arial, Helvetica, sans-serif" 
                            vAlign=top align=left height=61>


</TD></TR></TBODY></TABLE><BR>
                        <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>

					<?=$content?>

</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
                        <P><BR></P>
                        <TABLE valign=down class=pc500 cellSpacing=6 cellPadding=0 width=510 
                        align=center bgColor=#ffffff border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-WEIGHT: bold; FONT-SIZE: 11px; COLOR: #949aab; FONT-FAMILY: Arial, Helvetica, sans-serif" 
                            vAlign=top align=left><IMG height=16 
                              src="img/discuss.gif" 
                              width=14> При використанні матеріалів даної сторінки посилання обов'язкове!
<br><br>
<!--
<script type="text/javascript">(function() {
  if (window.pluso)if (typeof window.pluso.start == "function") return;
  if (window.ifpluso==undefined) { window.ifpluso = 1;
    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
    var h=d[g]('body')[0];
    h.appendChild(s);
  }})();</script>
<div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=08" data-services="vkontakte,odnoklassniki,facebook,twitter,google,moimir,email,print"></div>
-->

<br><br>


<!-- SpyLOG 
<script src="http://tools.spylog.ru/counter_cv.js" id="spylog_code" type="text/javascript" counter="927890" part="" track_links="ext" page_level="0">
</script>
<noscript>
<a href="http://u9278.90.spylog.com/cnt?cid=927890&f=3&p=0" target="_blank">
<img src="http://u9278.90.spylog.com/cnt?cid=927890&p=0" alt="SpyLOG" border="0" width="88" height="31"></a>
</noscript>
 SpyLOG -->

<!-- Рейтинг винницких сайтов , code for http://inrtzp.vntu.edu.ua -->

<script type="text/javascript">java="1.0";java1=""+"refer="+escape(document.referrer)+"&page="+escape(window.location.href); document.cookie="astratop=1; path=/"; java1+="&c="+(document.cookie?"yes":"now");</script>
<script type="text/javascript1.1">java="1.1";java1+="&java="+(navigator.javaEnabled()?"yes":"now")</script>
<script type="text/javascript1.2">java="1.2";java1+="&razresh="+screen.width+'x'+screen.height+"&cvet="+(((navigator.appName.substring(0,3)=="Mic"))? screen.colorDepth:screen.pixelDepth)</script>
<script type="text/javascript1.3">java="1.3"</script>
<script type="text/javascript">java1+="&jscript="+java+"&rand="+Math.random(); document.write("<a href='http://rating.vn.ua/?fromsite=212'><img "+" src='http://rating.vn.ua/img.php?id=212&"+java1+"&' border='0' alt='Рейтинг винницких сайтов' width='88' height='31'><\/a>");</script>
<noscript><a href="http://rating.vn.ua/?fromsite=212" target="_blank"><img src="http://rating.vn.ua/img.php?id=212" border="0" alt="Рейтинг винницких сайтов" width="88" height="31"></a></noscript>

<!-- /Рейтинг винницких сайтов -->



</TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD></TR></TBODY></TABLE></TD>
          <TD vAlign=top align=right width=57><IMG height=221 
            alt="" src="img/verh7.gif" 
        width=56></TD></TR>


        <TR>
          <TD vAlign=top align=left width=188 
          background="img/fon3.gif" 
          bgColor=#69b3da>&nbsp;</TD>
          <TD vAlign=top align=left width=555 bgColor=#ffffff>
            <TABLE cellSpacing=6 cellPadding=0 width=400 border=0>
              <TBODY>
              <TR>
                <TD class=srsm align=left>

</TD>
                <TD class=srsm align=right></TD></TR></TBODY></TABLE></TD>
          <TD vAlign=top align=right width=57>&nbsp;</TD></TR>



        <TR vAlign=bottom>
          <TD vAlign=top align=right width=188 
          background="img/a1.gif" 
          bgColor=#6ab4db height=65>
            <TABLE cellSpacing=0 cellPadding=0 border=0>
              <TBODY>
              <TR bgColor=#ffffff>
                <TD align=right><IMG height=31 
                  src="img/spaker.gif" 
                  width=1></TD>
                <TD></TD></TR>
              <TR bgColor=#ffffff>
                <TD align=right><IMG height=38 
                  src="img/ugol26.gif" 
                  width=52></TD>
                <TD><IMG height=1 
                  src="img/spaker.gif" 
                  width=10></TD>

</TR></TBODY></TABLE></TD>
          <TD align=right width=555 bgColor=#ffffff height=65>
            <TABLE cellSpacing=0 cellPadding=0 border=0>
              <TBODY><tr><td align=center>
Факультет інфокомунікацій, радіоелектроніки та наносистем ФІРЕН &copy; 1999-<script> var date = new Date(document.lastModified);
a=Math.round(date.getMonth())+1
document.write(+date.getFullYear())
</script>
</TD>

                <TD align=right><IMG height=38 
                  src="img/ugol222.gif" 
                  width=52></TD>
</TD></tr></TBODY></TABLE></TD></tr></TBODY></TABLE>
<center><font class=lang1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
Розробка та підтримка: Володимир Бєлов &copy; 2001-<script> var date = new Date(document.lastModified);
a=Math.round(date.getMonth())+1
document.write(+date.getFullYear())
</script>




</BODY></HTML>
