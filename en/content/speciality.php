<?php
	$title = "SPECIALITIES";
?>

                        <TABLE class=pc500 id=table25 height=63 cellSpacing=1 
                        cellPadding=0 width=510 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-WEIGHT: normal; FONT-SIZE: 22px; COLOR: #104294; FONT-FAMILY: Arial, Helvetica, sans-serif" 
                            vAlign=top align=left height=61>


</TD>                     <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>

Institute of Radio Engineering, Communications and Instrumentation Vinnytsia National Technical University provides training for full-time and correspondence courses for those specialties and specializations:<br><br>

<TABLE WIDTH=507 BORDER=1 BORDERCOLOR="#00b8ff" CELLPADDING=4 CELLSPACING=3>
									<COL WIDTH=74>
									<COL WIDTH=406>
									<THEAD>
										<TR VALIGN=TOP>
											<TD ROWSPAN=4 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B><SPAN STYLE="font-style: normal"><FONT FACE="Arial, sans-serif"><FONT COLOR="#ff3333">6.050901</FONT></FONT></SPAN></B></FONT></FONT></FONT></P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>������������</B></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.56cm; margin-right: 0.01cm; text-indent: -0.32cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050901-01
												&mdash; ������������</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.56cm; margin-right: 0.01cm; text-indent: -0.32cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050901-02
												&mdash; ���������� ����������, ���������� � ����������� </FONT></FONT></FONT>
												</P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.56cm; margin-right: 0.01cm; text-indent: -0.32cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050901-03
												&mdash; ���������� � �����</FONT></FONT></FONT></P>
											</TD>
										</TR>
									</THEAD>
									<TBODY>
										<TR VALIGN=TOP>
											<TD ROWSPAN=3 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>6.050902</B></FONT></FONT></FONT></P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>����������������
												��������</B></FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.62cm; margin-right: 0.01cm; text-indent: -0.29cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050902-01
												&mdash; ������������ ����������� �������</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.62cm; margin-right: 0.01cm; text-indent: -0.29cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050902-02
												&mdash; �������������� � ����������� �������� � �������</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR VALIGN=TOP>
											<TD ROWSPAN=4 WIDTH=74>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>6.050903
												</B></FONT></FONT></FONT>
												</P>
											</TD>
											<TD WIDTH=406>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.14cm; margin-right: 0.01cm; font-style: normal">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><B>����������������</B></FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.54cm; margin-right: 0.01cm; text-indent: -0.26cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050903-01
												&mdash; �������������������� ������� � ����</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.54cm; margin-right: 0.01cm; text-indent: -0.26cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050903-02
												&mdash; ���������� � �������� ����������������</FONT></FONT></FONT></P>
											</TD>
										</TR>
										<TR>
											<TD WIDTH=406 VALIGN=TOP>
												<P ALIGN=JUSTIFY STYLE="margin-left: 0.54cm; margin-right: 0.01cm; text-indent: -0.26cm; font-weight: medium">
												<FONT COLOR="#ff3333"><FONT FACE="Arial, sans-serif"><FONT SIZE=1 STYLE="font-size: 8pt">6.050903-03
												&mdash; ���������� � �����</FONT></FONT></FONT></P>
											</TD>
										</TR>
									</TBODY>
								</TABLE>



<br>In these fields also are trained professionals with in-depth study of foreign languages ??at the level of technical translator. <br><br>


<b><center>Radio-direction </center></b><br>

"Radio"
Radio engineers engaged in the development and operation of modern audio and video appliances, transmission and reception of information, as well as radio systems for special applications.  <br><br>

"Radio communication equipment, radio and television"
Engineers this specialty radio operators can work in businesses and organizations that are engaged in the development and modernization of radio communications and broadcasting, their maintenance and repair.
 <br><br>

<b><center>Direction of electronic devices </center></b><br>

"Production of electronic products"
Students of this specialty study modern computer-aided design, engineering and manufacture of computers and microprocessor-based systems for all branches of industry and agriculture.  <br><br>

"Bioengineering and Medical Devices and Systems"
Graduates of this specialty work in medical and health-spa facilities, public and private companies both in Ukraine and abroad and engaged in the development of computed tomography and automated dispensary control problems microprocessor control measurements to provide medical information in the processed form. <br><br>

<b><center>Direction of Telecommunications</center></b><br>

"Telecommunication systems and networks"
Telecommunications engineers engaged in developing, upgrading, maintenance and operation of cellular, paging, trunk and satellite communications, telephone exchanges departmental, municipal, national and international communications, cable, satellite, microwave and fiber-optic networks.  <br><br>

"Technologies and Telecommunications"
Engineers Electronic Instrument this specialty work in government and commercial establishments engaged in the development, maintenance and repair of digital transmission systems, computer networks, mobile communications systems, telemetry systems and remote control systems, cable and satellite TV.
 <br><br>

"Office equipment and communication"
Preparing Engineers telekomunikatsionschikov this specialization is carried out only in VNTU. Specialists engaged in the development of this specialty and service copiers and printers, trading and banking technology, e-mail and telephone facilities and document telecommunications, personal computers and the "Internet". <br><br>
	


</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
