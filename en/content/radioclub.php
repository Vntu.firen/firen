<?php
	$title = "HAMRADIO STATION UR4NWJ";
?>
                        <TABLE class=pc500 id=table26 cellSpacing=6 
                        cellPadding=0 width=514 align=center bgColor=#ffffff 
                        border=0>
                          <TBODY>
                          <TR>
                            <TD class=pc500 
                            style="FONT-SIZE: 11px; COLOR: #000000; FONT-FAMILY: Verdana, Arial, Helvetica, sans-serif" 
                            vAlign=top align=Justify> <FONT color=#104294><FONT 
                              class=content><SPAN class=postcolor>


The radio station of the present VNTU (Vinnytsia National Technical University) in today is one of the oldest hamradio station in Vinnitsa.
<br><br>
It all started with the fact that back in 1972 in the second building svezhepostroennom then branch of the Kiev University of Technology has been allocated space, ie laid the first stone at <foundation> collective radio UK5NAJ. A small but very enthusiastic group of students built, it can be said for their enthusiasm, serious BEC and the antenna in record time.
<br><br>
There were present and the radio station RSB-70, hook-crook retrieved from the Department of Radio Engineering, and a homemade radio station on 10 meter range with amplitude modulation, collected Sergey Belov (UB5DZF, now UT5NB), and a tank connected transceiver 10RT, Valeria Vashkevich, and a lot of other equipment. On the antennas already and can not speak - they speak for themselves (Hi!).
<br><br>
It is a beginner team UK5NAJ, consisting of Vladimir Zlotnik (UB5DZA), Anatoly Homchuka (UB5DZB), Sergey Belov (UB5DZF) and collective UB5KWI, at a time when working with portable stations are strictly forbidden, it was possible to hold the first in an expedition to the island Festival (now Kemp's Island). Were conducted with distant stations from Chelyabinsk, Murmansk, Bereznyakov. Repeat such an expedition to the island of the same hams were only able to Vinnitsa in many more than thirty years, when it is not only not forbidden, but also most welcome.
<br><br>
Government decree in January 1974 on the basis of the CTS branch was opened Vinnitsa Polytechnic Institute, and with it a new building kollektivki, to relocate to the area of the assembly hall of the institute and <thrown> Delta at high tube boiler Institute. After some time, and changed the callsign. Now on the air sounded new - UB4NWJ. The station is actively working on the air, took part in competitions and confirmation to dozens of diplomas and thousands of QSL cards. And soon the station relocated to the 9th floor main building of the Institute, with which the roof in all directions looking chic in all ranges of the delta. But after the reforms have changed kollektivki and call sign, which is still used. The station has earned the callsign UR4NWJ. But there were circumstances so that by the mid 90s of last century the station, now Vilnius Gediminas Technical University ceased its operation.
<br><br>
Revival of the station, as well as in those ancient times, have engaged students of the Faculty of Radio Electronics. Had to start from the beginning, but that did not stop, and not for a moment gave rise to doubts about his chosen path.
<br><br>
Headed by the chief radio officer Department of Radioelectronics Alexander Tymoshchuk (US0NW), team composed of students of the Faculty: Vladimir Belov (UR5NBC), Alexander Pastuckh (UR3CJP) and a few beginners radio started to work on registration of normative documents, construction equipment and antennas. And at the end of 2004 at the Department of Telecommunication systems Television and Telecommunications Department of Radio Engineering Institute of Radio Engineering, Communications and Instrumentation VNTU again otrylas collective radio station.
<br><br>
Manufacture of radio, antenna-feeder and power devices completely fell on the shoulders of the collective stations, three months after the team coped with the task, and at least since the beginning of 2005 on the HF bands have performed the first ten links. Also on the basis of industrial VHF radio station was built at 144-146 MHz, which is working actively with radio city and region.
<br><br>
In the near future at the station antenna is installed bude on DELTA HF, half-wave vertical dipole at 28 MHz and the new and improved vertical antenna at 145 MHz. On the base station will run a batch cluster and amateur BBS, which will allow hams city and the region to learn the latest news, share information, etc.
<br><br>
And in the near future, the organization of extracurricular activities with students of higher rates in order to give a general idea of radiosporte, amateur HF and VHF communications bachelors and masters especially radio fields. And as a corollary to ensure the development radiosporta using their knowledge from the academic institution and provide them with hams from an organizational and technical point of view, even taking into account that not all students who attend these optional classes in the future will be engaged in amateur radio. Indeed, to many fans, and repeatedly had to deal with a complete lack of understanding of the issue, addressing both the sponsorship and organizational assistance. Understanding of the issue, ultimately, should lead to an increase in the number of people involved in amateur radio, and radiosportom people choose to work in television.
<br><br>
From the perspective of the educational process is vital to students' knowledge and training in later life and amateur radio radiokonstruirovaniem.
<br><br>
But to stop the progress is not worth it. Before the team is new even more serious problem - and doukomplektatsiya pereoborudyvanie stations with modern equipment to design, build and conduct amateur radio communications, which will bring a new qualitative stage of development of our kollektivku.
<br><br>
Today, collective radio UR4NWJ is one of the few kollektivok that are in the area. The station should provide an impetus to the development of youth radiosporta among students. This is not just a problem of collective - it is his duty, as it is the only amateur radio collective in the Vinnytsia region, open for higher education.
<br><br><p align=right><i><b>
Assistant kaf.TKSTV Belov Vladimir (call UR5NBC)<br></b>
Based on the article "� ����� ����!", Journal of '�������� �� � ���' 2'2006 </i></p>
</SPAN></FONT></FONT></TD></TR></TBODY></TABLE>
