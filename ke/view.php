<html>
<head>
	<meta content="text/html; charset=cp1251" http-equiv="content-type"/>
	<title><?=$title?></title>
	<script type="text/javascript" src="<?=$host?>jcarousel/lib/jquery-1.4.2.min.js"></script>
	<script type="text/javascript" src="<?=$host?>jcarousel/lib/jquery.jcarousel.min.js"></script>
	<link type="text/css" rel="stylesheet" href="<?=$host?>jcarousel/style.css">
	<link type="text/css" rel="stylesheet" href="style.css">
	<script type="text/javascript">

<?php
function localize($phrase) {
    /* Static keyword is used to ensure the file is loaded only once */
    static $translations = NULL;
    /* If no instance of $translations has occured load the language file */
/*    if (is_null($translations)) {
        $lang_file = . '/lang/' . LANGUAGE . '.txt';
        if (!file_exists($lang_file)) {
            $lang_file = INCLUDE_PATH . '/lang/' . 'en-us.txt';
        }
        $lang_file_content = file_get_contents($lang_file);*/
        /* Load the language file as a JSON object and transform it into an associative array */
/*        $translations = json_decode($lang_file_content, true);
    }
    return $translations[$phrase];*/
}
?>

function mycarousel_initCallback(carousel)
{
	// Disable autoscrolling if the user clicks the prev or next button.
	carousel.buttonNext.bind('click', function() {
		carousel.startAuto(0);
	});
	
	carousel.buttonPrev.bind('click', function() {
		carousel.startAuto(0);
	});
	
	// Pause autoscrolling if the user moves with the cursor over the clip.
	carousel.clip.hover(
		function() {
			carousel.stopAuto();
		}, 
		function() {
			carousel.startAuto();
		}
	);
};
 
		$(document).ready(function(){
			$("#edition-carousel").jcarousel({
				size: 15,
				auto: 2,
				animation: 1000,
				wrap: "last",
				initCallback: mycarousel_initCallback,
				CombinedClasses: true
			});
			$("#scien").bind("click", function() {
				$(".btn_sub_wrapper").toggle("slow");
				return false;
			});
		});
	</script>
</head>

<body>
	<div class="main_wrapper">
		<div class="main">
			
			<div class="header">
				<div class="title">
					<h1><?=$title?></h1>
				</div>
			</div>
			
			<div class="content_main">
				
				<div class="menu">
					<div class="lang">
						&nbsp;&nbsp;<a class="site_vntu" href="http://www.vstu.edu.ua/ua/" target="_blank">&laquo; �������� ���� ����</a>
						<div style="float: right; margin-right: 10px;">
						<a href="#" onclick="window.location='<?=$host?>index.php?l=ua';"><img src="<?=$host?>img/ukraine.png" border="0"></a>&nbsp;
						<a href="#" onclick="window.location='<?=$host?>index.php?l=ru';"><img src="<?=$host?>img/russia.png" border="0"></a>&nbsp;
						<a href="#" onclick="window.location='<?=$host?>index.php?l=eng';"><img src="<?=$host?>img/english.png" border="0"></a>
						</div>
					</div>
					<div class="empty"></div>
					
					<div class="btns">
						
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>">
							�������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&history">
							������ �������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&industry">
							��� ������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&teachers">
							��������� �� �����������
						</a></div>
						<div class="btn" id="scien"><a href="<?=$host?>?l=<?=$lang?>&scientific_research">
							������� ������
						</a></div>
							<div class="btn_sub_wrapper" style="display: none;">
								<div class="btn_sub">
									&raquo; <a href="<?=$host?>?l=<?=$lang?>&scientific_research&page=1">������� ������</a>
								</div>
								<div class="btn_sub">
									&raquo; <a href="<?=$host?>?l=<?=$lang?>&scientific_research&page=2">����������� �����</a>
								</div>
								<div class="btn_sub">
									&raquo; <a href="<?=$host?>?l=<?=$lang?>&scientific_research&page=3">������������</a>
								</div>
								<div class="btn_sub">
									&raquo; <a href="<?=$host?>?l=<?=$lang?>&scientific_research&page=4">������ � ������������</a>
								</div>
							</div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&discipline">
							���������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&international_communication">
							̳�������� ��'����
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&abiturient">
							��������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&useful_materials">
							������� ��������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&editions">
							�������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&news">
							������
						</a></div>
						<div class="btn"><a href="<?=$host?>?l=<?=$lang?>&contacts">
							��������
						</a></div>
					</div>
					
				</div>
				
				<div class="content">
					<div class="start"></div>
					<?=$content?>
					<div class="clr"></div>
				</div>
				
			</div>
			
			<div class="footer">
				<div class="copyright">
					&copy; ���� 2012
				</div>
			</div>
			
		</div>
	</div>
</body>

</html>