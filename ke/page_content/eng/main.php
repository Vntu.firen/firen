<?php
	$title = "�������";
?>

<div class="h1">The department of electronics </div>

<img src="img_content/main/1.jpg" align="left" width="130">
	
	<p class="no_indent">
		<strong>
		Cheaf of department � Doctor of Science, prof.  <br>
		<span class="h2">Bilynskiy Yosyp Yosupovych  </span> r. 1226, Ukraine, Vinnytsia, Khemelnitske shose st., 95 <br>
		Tel.: (0432) 59-80-13  <br>
		E-mail: el@inmt.vstu.vinnica.ua
		</strong>
	</p>  
	_______________________________________________________________
	
	<p>
	The department of electronics performs specialist training for theoretical and practical activities in the field of electronics: development of microelectronic devices with use of automatic development systems, development of specialized electronic devices.
	</p>
	
	<br>

<p>
Our goals:
<ul>
	<li>Satisfy the demand of school leavers in qualitative higher education is the field of electronics and microelectronics.</li>
	<li>Training engineers for industrial enterprises, energetic systems of Ukraine, small and medium business enterprices, social activities, that have high level of professional knowledge and skills, can competently use their skills and knowledge in practice for engineer work solving complicated technical tasks in the field of informatics and controlling technics.</li>
	<li>Train specialists that are ready for permanent self improvement in the field of electronics, that are with its dynamic improvement, knowing foreign languages, modern computer technologies, innovations, facilities of network informational resourses, knowing basic business principlesand industrial economics,ecology and safe life activity.</li>
	<li>Keep the high level of natural sciences knowledge, promote training school leavers, based on common society values, forming mental outlook, erudition, involving specialists to cultural and democratic traditions of society.</li>
</ul>
</p>

<p>The department of electronics for its continuous activity period is considered to be one of the most innovatory and progressive departments in our university. Great number of scientists, well known over the world worked for developing of the department. It can be regarded as the basis for developing of number of other university departments, in different times of the university history. The chief of the department is doctor of science, professor Bilynskyy Yosyp Yosypovych. Departmnet has graduated more than 1200 engineers since the its founding in 1966. Most of which continue to enhance the prestige of the speciality. 5 doctors of sciences and more than 22 Ph.d have graduated the department. The graduates of the department usually occupy executive position in the organization. Such success is not a good opportunity, but a result of laborious work of students and their proffesors. The training is managed in the well known all over the world scientific school under control of the doctor of science, professor, ac academician of the Ukrainian engineer academy, honoured science and technics worker, Osadchuk Volodymyr Stepanovych, who was the chief of the electronics department from 1966 to 2009.</p>
 
<p>13 lecturers work in the department. Among them 2 are doctors of science, proffessors (Osadchuk V.S., Bilynskyy Y.Y.), 8 are Ph.d, associated proffesors (Prokopov �.D., Kravchenko Yu.S., Krylyk L.V., Prokopova M.�., Martynuk V.V., Selets�ka �.�., Ratushnyy P.M., Ogorodnyk K.V.), 3 are assistents  (Kravchenko S.Yu., Ilchenko O.M., Melnychuk A.O.)</p>

<p>The department maintains tight scientific relations with the Higher educational establishments of Ukraine, countries of CIS, universities of Poland, Lithuania and others. The department manages international science-technical co-operation participating in international conferences, exhibitions, symposiums, different international scientific programs, grants, exchanging scientists.</p>

<p>Ph.ds are trained in the postgraduate study. Students of the department are involved in the scienfic research activities, increasing their own knowledge level. There are good conditions for students to make the scientific research and education.</p>




<div class="h1">Specialties and specializations</div>

<p>The department manages specialists training in such directions:</p>

<p><strong>Micro- and nanoelectronics </strong> specialty - <strong>Microelectronics and semiconductor devices</strong> specializations:
<ul>
	<li>Microelectronic and semiconductor devises in computer systems of diagnostics and control in transport;</li> 
	<li>Scheme and computer electronics</li>
</ul></p>

<p><strong>6.050802 - Electronic devices and systems</strong> specialty - <strong>Electronic devices and systems </strong> specializations:
<ul> 
	<li>Automobile and energy-efficient electronics;</li>
	<li>Electronic devices and systems in the energy supply systems in electrical railway transport and transmission lines of direct current.</li>
</ul><p>




<img src="img_content/main/2.jpg" align="left">

<p>Students of microelectronics and semiconductor devices speciality master fundamental knowledges in physics-mathimatic sciences and programming, physics of processes in electronic devices, mathematical design, and also automated developing of modern microelectronic devices, built on and computer-assisted systems with the usage of modern nano-, micro- and optical technologies, microprocessor technique and programmable logic.</p>
  
<p>Got base education allows successful working to graduates in research establishments, in all spheres of business, on industrial enterprises, work, related to development, production, marketing and exploitation of microelectronic devices, computer-assisted systems, systems of parameters transformation of electric energy, apparatus of mobile communication.</p> 
<div class="clr"></div>



<img src="img_content/main/3.jpg" align="right">

<p>Students of electronic devices and devices speciality seize digital and analog circuit technology, sensors and microprocessor technique, modern facilities of planning and development of difficult electronic devices and systems, in particular, instrumentation, technological, ecological, energy effective, managing, in transport, and systems of electric energy transformation, connection, systems of reflection, treatment and priv.</p>

<!--<div class="clr"></div>-->



<!--<img src="img_content/main/4.jpg" align="left">-->

<p>Graduates successfully work on state and joint-stock enterprises in research establishments, service centers, which are specialized in the field of marketing, production, editing and exploitation of electronic and computer technique, automation, domestic apparatus, facilities of telecommunications.</p>

<!--<div class="clr"></div>-->


<p>The department of electronics carries out preparation of specialists for work of both research and practical activities, character practically in all areas of electronics: from development of electronic and microelectronic devices with the use of computer-aided designs to development of the dedicaded electronic systems of the different setting.</p>

<!--<div class="clr"></div>-->



<p>In times of the existence a department is legally considered to be one of the innovative and progressive subsections in our university. The founders of department were scientists who worked and work not only in Ukraine, but also in other countries of the world. It can be regarded as the basis for developing of number of other university departments, in different times of the university history.</p>
<!--<div class="clr"></div>-->