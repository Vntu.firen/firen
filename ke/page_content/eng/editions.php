<?php
	$title = "Publications";
?>

<ol>
	<li>Osadchuk �.V. Microelektronic frequency converters of pressure based on transistor structures with negative resistance.�Vinnytsia: �Universum-Vinnytsia�, 2000.�303p.</li> 
	<li>Osadchuk �.V. Photosensitive converter based on structures with negative resistance. � Vinnytsia: Continent, 1998. �130 �.</li> 
	<li>Novikov �.�. Mystery of the profit Ilia�s fire egg: description of plasma theory of the globular discharge. � Vinnytsia: Continent-PRIM, 1999. � 232 p.</li> 
<!--	<li>Semiconductor devices with negative capacitance. workbook (VNTU) Osadchuk V.S., Osadchuk �.V. � 2006</li> 
	<li>Fibre optic transmitting systems. workbook (VNTU) Osadchuk V.S., Osadchuk �.V. � 2005</li> 
	<li>Fibre optic information transmitting systems. Laboratory workbook. (VNTU) Osadchuk V.S., Osadchuk �.V. � 2005</li> 
	<li>Basic physics of microelectrnic devices. Workbook. Osadchuk V.S. - 1991</li> 
	<li>Inductive method in semiconductor devices. Osadchuk V.S. � 1987</li>--> 
	<li>Osadchuk V.S. Basics of functional microelectronics, - 1998</li> 
	<li>Osadchuk �.�. Osadchuk �.�. Semiconductor diods. Workbook. � 2002</li> 
	<li>Osadchuk �.�. Osadchuk �.�. Transistors. Workbook. -2003</li> 
	<li>Osadchuk �.�. Osadchuk �.�. Semiconductor information converters. Workbook. � 2004.</li> 
	<li>Methodical workbook and program of state examination in fundamental and engineering training for students of specialities: �Microelectronics and semiconductor devices� and �Electronic devices� �.I. Zhmurko, Y.S.Kravchenko, V.�.Revenok, V.S. Osadchuk. � 2002</li> 
	<li>Methodical workbook for laboratory training in vacuum technics for students of speciality: �Electronic devices�.  �.�. Novikov, �.�.Starovoytov, �.V. Shubin � 2008</li> 
	<li>Methodical workbook for yearly project in calculus mathematics for students of specialities:  �Microelectronics and semiconductor devices� and �� �Electronic devices�  Krylyk L.V., Prokopova �.�. � 2008</li> 
	<li>Methodical workbook for diploma paper for students of specialities: �Microelectronics and semiconductor devices� and �Electronic devices� Y.S.Kravchenko. � 2007</li> 
	<li>Laser technics and technology. Laboratoryworkbook (VNTU) Novikov �.�., Shubin �.V. � 2006</li> 
	<li>Basis of designing systems automatisation of radioelectronics devices.Workbook. Prokopov I.D. - 2000</li> 
	<li>Basis of automatic designing systems of radioelectronics devices.Workbook. Prokopov I.D. - 2005</li> 
	<li>Bilynskiy Y.Y., Motygin V.V. Information processing with the computer mathematics facilities.</li> 
	<li>Bilynskiy Y.Y. Devices of communication.</li> 
	<li>Bilynskiy Y.Y. Interface devices of computer systems.</li> 
</ol>
<ul id="edition-carousel">
	<li><img src="img_content/editions/1.jpg" align="left"></li>
	<li><img src="img_content/editions/3.jpg" align="left"></li>
	<li><img src="img_content/editions/4.jpg" align="left"></li>
	<li><img src="img_content/editions/5.jpg" align="left"></li>
	<li><img src="img_content/editions/8.jpg" align="left"></li>
	<li><img src="img_content/editions/9.jpg" align="left"></li>
	<li><img src="img_content/editions/10.jpg" align="left"></li>
	<li><img src="img_content/editions/11.jpg" align="left"></li>
	<li><img src="img_content/editions/12.jpg" align="left"></li>
	<li><img src="img_content/editions/13.jpg" align="left"></li>
	<li><img src="img_content/editions/15.jpg" align="left"></li>
	<li><img src="img_content/editions/16.jpg" align="left"></li>
	<li><img src="img_content/editions/17.jpg" align="left"></li>
	<li><img src="img_content/editions/18.jpg" align="left"></li>
	<li><img src="img_content/editions/19.jpg" align="left"></li>
</ul>
