<?php
	$title = "Scientific researches";
?>

<?php
	switch ($_GET['page']) {
		case '1':
?>

<img src="img_content/scientific_research/1.jpg" align="left">

<p>
Department conducts scientific research works in optical-electronic measuring transformers and informative measuring systems; study of the physical phenomena in solids, gases and semiconductor structures; research of physical and chemical properties of non-equilibrium plasma developments of electronic devices for its diagnostics and control of plasma-chemicalprocesses; designing of digital filters using the programmable logical matrices; developments of methods and apparatus for analog and digital signals processing in radioelectronics and sonars, commutation, vibroacoustics; development of microelectronic frequency touch-controls of humidity, pressure, gas, magnetic field, based on structures with subzero resistance.  
</p>
<div style="height: 50px"></div>

<p>
Department effectively works in a science. Masters, ph.d and doctors are trained in the department. Scientists of the department have 14 monographs, 18workbooks, more than 250 scientifec articles and patents, 55 lectures at international scientific conferences, 6 doctors of sciences and 25 ph.ds were trained in our department. . 
</p>

<div style="float: left; margin-right: 15px;">
	<img src="img_content/scientific_research/_1.jpg" height="250">
	<div style="font-size: 11px; text-align: center;">The meeting advanced seminar of the Department</div>
</div>
<div style="float: left;">
	<img src="img_content/scientific_research/2.jpg" height="250">
	<div style="font-size: 11px; text-align: center;">Prof.. Osadchuk V.S. with their manuals, <br> patents </div>
</div>
<div class="clr"></div>
<div style="float: left; margin-right: 15px;">
	<img src="img_content/scientific_research/_2.jpg" height="250">
	<div style="font-size: 11px; text-align: center;">The meeting advanced seminar of the Department of <br> on the the previous PhD dissertation <br> Ratushnogo P.M.</div>
</div>
<div style="float: left;">
	<img src="img_content/scientific_research/_3.jpg" height="250">
	<div style="font-size: 11px; text-align: center;">The meeting advanced seminar of the Department of <br> on the the previous PhD dissertation <br> Seleckoy O.O.</div>
</div>
<div class="clr"></div>


<center>
	<div class="h1">In scientific school were trained</div>
	<strong>Doctors of sciences</strong>
</center>
	<div style="width: 200px; float: left; margin-left: 200px;">
		<ol>
			<li>Filyniuk �.�.</li> 
			<li>Molchanov P.�.</li>
			<li>Kychak V.�.</li>
		</ol>
	</div>
	<div>
		<ol start="4">
			<li>Osadchuk �.V.</li> 
			<li>Novikov �.�</li>
		</ol>
	</div>
<div class="clr"></div>	

<br>	
<center>
	<strong>Ph.ds in electronics</strong>
</center>
	<div style="width: 200px; float: left; margin-left: 70px;">
		<ol>
			<li>Filyniuk �.� </li> 
			<li>Molchanov P.�. </li>
			<li>Kychak V.�.  </li>
			<li>Yarovenko �.G. </li>
			<li>Prokopov �.D. </li> 
			<li>Dzuban V.G. </li> 
			<li>Krylyk L.V. </li>
			<li>Semerenko �.�.</li>
		</ol>
	</div>
	<div style="width: 250px; float: left;">
		<ol start="9">
			<li>Palamarchuk Y.�.</li> 
			<li>Yaremchuk V.F.</li>
			<li>Revenok V.�.</li>
			<li>Osadchuk Y.V.</li>
			<li>Kravchenko Y.S.</li> 
			<li>Prokopova �.�.</li> 
			<li>Odobets�kiy S.�.</li>
			<li>Gykavyy V.�.</li>
		</ol>
	</div>
	<div>
		<ol start="17">
			<li>Volynets� V.�.</li> 
			<li>Osadchuk �.V.</li>
			<li>Tarnovs�kyy �.G.</li>
			<li>Kravchuk N.S.</li>
			<li>Martynuk V.V.</li> 
			<li>Yushchenko Y.�..</li> 
			<li>Bilokon� N.L.</li>
			<li>Selets�ka �. �.</li>
			<li>Ratusnyy P.M.</li>
		</ol>
	</div>
<div class="clr"></div>	


<?php
		break;
		case '2':
			$title = "Publications";
?>

<!--<div class="h1">����������� ������ �����</div>-->

<ol>
	<li>Bilynskyy Y. Y. Sharpness increasing of fuzzy 2-D images / Bilynskyy Y. Y., Ratushnyy P. �. // Visnyk of Vinnytsia technical institute.�2009. � �6. � P. 12�15.<div><a href="http://visnyk.vstu.vinnica.ua/2009/6/pdf/09byycti.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Bilynskyy Y. Y. Method of subpixel object edge determination based on low-frequency filtering / Bilynskyy Y. Y., ������ �.�. // Visnyk of Vinnytsia technical institute. � 2008. � �3. � P. 5�8.<div><a href="http://visnyk.vstu.vinnica.ua/2008/3/pdf/08byyonf.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Bilynskyy Y. Y. Filtering impulse noise on 2-D images / Bilynskyy Y. Y. Ratushnyy P. �. // Optical-electronic informational-energetic technologies. � 2007. � �1(13). � P. 30�34.<div><a href="http://www.vstu.vinnica.ua/~oeipt/archive_ua.html" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Bilynskyy Y. Y. Adaptive method of border filtering / Bilynskyy Y. Y., Krysak �. �., Klymenko �. V. // Visnyk of Vinnytsia technical institute. � 2007. � �5. � P. 116�118.<div><a href="http://visnyk.vstu.vinnica.ua/2007/5/pdf/07byypoz.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Bilynskyy Y. Y. Device and program for reconstruction of three-dimensional human back surface relief / Bilynskyy Y. Y., Yukish S.V. // Scientific works in VNTU. � 2010. � �1. � P. 1�7.<div><a href="http://www.nbuv.gov.ua/e-journals/vntu/2010_1/2010-1.files/uk/10yybdrr_ua.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Bilynskyy Y. Y. Problems and outlooks for visual methods of children hip joint diagnostics / Bilynskyy Y. Y., Mel�nychuk �.�., Mel�nychuk �.V. // Scientific works in VNTU. � 2009. � �4. � P. 1�4.<div><a href="http://www.nbuv.gov.ua/e-journals/vntu/2009-4/2009-4.files/uk/09jjbhjp_ua.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>�Osadchuk V. S. Optical microelectronic converter with active inductivity element for plasma-chemical process control / Osadchuk V.S., Osadchuk �. V., Kravchenko Y. S., Selets�ka �. �. // Visnyk of Vinnytsia technical institute.�2009. � �6. � P. 94�100.<div><a href="http://visnyk.vstu.vinnica.ua/2009/6/pdf/09ovsopp.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Osadchuk V. S. Photoreactive effect in MDS-transistor structures with two-directional channel lighting / Osadchuk V. S., Osadchuk O.V., Il�chenko �. �., Baraban S. V. // Visnyk of Vinnytsia technical institute.�2008. � �4. � P. 92�98.<div><a href="http://visnyk.vstu.vinnica.ua/2008/4/pdf/08ovsdok.pdf" target="_blank">Full text in PDF format&raquo;</a></div></li>
	<li>Osadchuk V. S. Microelectronic frequency converter of magnetic field with active inductivity element / Osadchuk V. S., ������� �. �. // Visnyk of Vinnytsia technical institute.�2007. � �2. � P. 92�96.<div><a href="http://visnyk.vstu.vinnica.ua/2007/2/pdf/07ovsaie.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Osadchuk V. S. Converters of optical power based on field transistors with two-directional channel lighting / Osadchuk V. S., �. �. �������� // Scientific works in VNTU. � 2009. - �3. � P. 1-7.  <div><a href="http://www.nbuv.gov.ua/e-journals/vntu/2009-3/2009-3.htm" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Kravchenko Y.S. Diagnostics and control of temperature in plasma-chemical processes of microstructures etching / V.S. Osadchuk, G.V. Ivchuk // Visnyk of KNU. � 2009. - �2. � P. 147-152.  <div><a href="http://www.nbuv.gov.ua/portal/natural/Vchnu/Tech/2009_2/zmist.files/36.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>
	<li>Prokopova �.�. Measuring of current-voltage characteristics of frequency converter of probe current / �.V. Osadchuk, D.P. Dudnik // Measuring and calculating technics in technological processes. � 2009. - �1. � P. 84-88. <div><a href="http://www.nbuv.gov.ua/portal/Natural/Vtot/2009_1/zmist.files/21.pdf" target="_blank">Full text in PDF format &raquo;</a></div></li>

<div class="h1">����������</div>
	<li>Osadchuk V. S., Osadchuk �. V., Semenov �. �. Electrical oscillations generators based on transistor structures with subzero resistance. Monograph 2009. - 184 p.</li> 
	<li>Novikov �.�. Mysteries of fire egg of Illia, the profit: explanation of plasma nature of fire-ball. � Vinnytsia: Continent-PRIM, 1999. �232 p.</li> 
	<li>Osadchuk V. S., Osadchuk �. V., Kravchuk N. S. Microelectronic sensors of temperature with frequency output. Monograph 2007. � 163 p.</li> 
	<li>Osadchuk V. S., Osadchuk �. V. Sensors of pressure and magnetic field. Monograph � Vinnytsia: Universum, 2005. - 207 p. </li> 
	<li>Osadchuk V. S., Osadchuk �. V., Krylyk L. V. Moisture sensors. Monograph 2003. - 208 p.</li> 
	<li>Osadchuk V. S., Osadchuk �. V., Verbytskiy V. G. Temperature and optical microelectronic frequency converters. Monograph 2001. - 195 p.</li> 
	<li>�Osadchuk V. S., Osadchuk �. V. Reactive properties of transistors and transistor circuits. Monograph 1999. - 275 p.</li> 
	<li>Osadchuk V. S., Osadchuk �. V., Prokopova �.�. Gas sensors. Monograph 2008. - 167 p.</li>
	<li>Bilynskyy Y. Y., Gorodets�ka �. S. Automated control of liquid surface tension using method of lying drop. Monograph 2008. - 146 p.</li>
	<li>Yukysh M.Y., Kuharchuk V. V., Bilynskyy Y. Y. Optical-electronic devices for rotatory movement control based on special modulation. Monograph 2009. - 138 p.</li>
-->
</ol>

<div style="margin-left: 140px;">
	<img width="100" height="140" align="left" src="img_content/scientific_research/3.jpg">
	<img width="100" height="140" align="left" src="img_content/scientific_research/4.jpg">
	<img width="100" height="140" align="left" src="img_content/scientific_research/5.jpg">
	<img width="100" height="140" src="img_content/scientific_research/6.jpg">
	<div class="clr"></div>
	<img width="100" height="140" align="left" src="img_content/scientific_research/8.jpg">
	<img width="100" height="140" align="left" src="img_content/scientific_research/7.jpg">
	<img width="100" height="140" align="left" src="img_content/scientific_research/_10.jpg">
	<img width="100" height="140" src="img_content/scientific_research/_11.jpg">
</div>

<?php
		 break;
		 case '3':
		 	$title = "INTRODUCTION OF SCIENTIFIC DEVELOPMENTS OF DEPARTMENT";
?>

<div class="h1">SYSTEM OF COMPUTER DIAGNOSTICS OF FOOT</div>

<p>
System of computer diagnostics of foot was developed by the developers team from the spine diagnostics-treatment center, highly skilled doctors and scientists of Vinnytsia national technical university with large experience of practical developments control and measuring and diagnostic apparatus for different applications: 
</p>

<ul>
	<li>Bilynskiy Y. Y., doctor of science, professor of VNTU</li>
	<li>Kovalenko Y.�., doctor, neuropathologist </li>
	<li>Yukysh S.V., post graduate student of VNTU;</li>
	<li>Yukysh M.Y., ph.d. in electronics , professor assistant of VNTU.</li>
</ul>

<img src="img_content/scientific_research/3_1.jpg" align="right" height="220">
<p>
System of the computer diagnostics of foot intended for the leadthrough of diagnostics of pathology feet and different diseases of locomotorium, looking after motion of treatment, and also for prophylactic researches.  
</p>
<p>
There is direct intercommunication in joints and spine between distributing of pressure under a foot in the process of circulation and origin of chronic pain. In the past, distributing of pressure was analysed by footprint of patient foot on glass or paper. This type of diagnostics did not allow directly to judge about the real parameters the partition of load under a foot. System of the computer diagnostics of foot, the built with application of computer methods and facilities of diagnostics allows to get more exact result. 
</p>
<p>
<b>application of the system of the computer diagnostics of foot </b>
</p>

<ul>
	<li>Orthopaedics-traumatology</li>
	<li>Pediatrics</li>
	<li>Exercise therapy</li>
	<li>Prosthetics</li>
	<li>Physiotherapy</li>
	<li>Manual therapy</li>
	<li>Professional pathology</li>
	<li>Osteopathy</li>
	<li>Other adjacent specialities</li>
</ul>

<p>
<b>Systems consists of: </b><br>
</p>

<ul>
	<li>Optical-electronic podoscope</li>
	<li>Computer with laser or ink-jet printer </li>
	<li>Software "PodPro"</li>
	<li>Doctor workplace</li>
</ul>

<p>
<b>Advantages of the system</b>
</p>

<ul>
	<li>Short time of diagnostics (1-5 min)</li>
	<li>Automated processing of research results</li>
	<li>Receipt of load partition gradation on a foot, its high contrast and sharpness</li>
	<li>Possibility of loading comparison of left and right feet</li>
	<li>Possibility of results comparison from different researches</li>
	<li>Report forming as a result of researches, that includes graphic and self-reactance information</li>
	<li>Low cost</li>
</ul>

<p>
<b>Technical parameters of computer diagnostics system </b>
</p>

<ul>
	<li>System works from alternating-current mains 220 V � 10%  and frequency 50 Hz</li>
	<li>Connection interface � USB, ���</li>
	<li>Maximally possible time of operating condition establishment of the system from the moment of switching on less than 15 min.</li>
	<li>Time of continuous work of the system not less than 8 hours a day</li>
</ul>


<div class="h1">Optical-electronic podoscope</div>

<p>
Optical-electronic podoscope is intended for the removal of imprints and determination of partition of man feet load. Farther, due to in-use software possibility of receipt of high-fidelity picture of load partition which considerably simplifies diagnostics of pathologies on the early stages of their development is arrived at.
</p>
<p>
Device is complected with: framework, surface for establishment of feet, that made from glass, source of optical radiation and power module, digital photocamera, cables of connection, with a computer, CU by an emitter.
</p>
	<div style="padding-left: 140px;">
		<img src="img_content/scientific_research/3_2.jpg" align="left" style="margin-right: 30px; margin-bottom: 0;">
		<img src="img_content/scientific_research/3_3.jpg">
		<div style="padding-left: 42px; padding-top: 5px;">
			The appearance of the device shown in Figure.
		</div>
	</div>



<div class="h1">Software "PodPro""</div>

<p>
<b>Software "PodPro" allows:</b>
</p>

<ul>
	<li>getting flat image and 3D image of foot;</li>
	<li>getting contour of foot;</li>
	<li>getting graphical presentation of load on feet;</li>
	<li>getting load center position acroos ech foot;</li>
	<li>getting foot load coefficient in percents related to the general load on feet;</li>
	<li>getting geometrical parameters of feet: length, area;</li>
	<li>getting characteristics, that describe structure and state of foot: Klark angle, isthmus length, isthmus index;</li> 
	<li>conducting electronic card index of patients.</li>
</ul>

<p>
<b>"PodPro" software solve problems</b>
</p>

<ul>
	<li>Protecting of image from overexposing from light outsourcings;</li>
	<li>Expansion of image intensity range to maximally possible (linear contrasting);</li>
	<li>Image noise filtering;</li>
	<li>Image counters detecting;</li>
	<li>graduation of image colors depending on loading feet;</li>
	<li>Getting feet parameters</li>
</ul>

<p>
	General view of the software "PodPro" shown in figure.
</p>
<div style="padding-left: 160px;">
	<img src="img_content/scientific_research/3_4.jpg">
</div>


<p>
Software "PodPro" is intended for work with operating systems: Windows 2000, 2003, XP. The program allows conducting feet scanner tuning in accordance with the individual features of patient, conducting research feet in the static mode, and in the research process to display necessary service information.  
</p>
<p>
the results of input image processing are shown on figures. 
</p>
<div style="padding-left: 110px;">
	<div style="float: left; margin-right: 50px;">
		<img src="img_content/scientific_research/3_5.jpg">
		<div style="text-align: center;">Result Trace Contour <br> input image </div>
	</div>
	<div style="">
		<img src="img_content/scientific_research/3_6.jpg">
		<div style="text-align: center; width: 474px;">The result of calibration <br> load feet </div>
	</div>
</div>



<p>
Sometimes there can be a necessity of the research results graphic editing, for this purpose the proper mode which provides necessary functionality is foreseen in software "PodPro". 
</p>
<p>
The results of patient research are represented in a table view (fig. 5) or as a report. A resulting report can be printered. Such report is formed automatically and contains necessary graphic, text and quantitative information about a patient and parameters which characterize the state of his feet. 
</p>


<div style="padding-left: 180px;">
	<img src="img_content/scientific_research/3_7.jpg">
	<div style="padding-left: 30px; padding-top: 5px;">
		Tabular presentation of survey results
	</div>
</div>
<p>
Software is foreseen by the conduct the account of patients. The card index of patients is collected in a database and allows its storage, and to look at over all conducted researches of every patient in a need. Such account allows the track dynamics of treatment /progressing of a disease. 
</p>

<div class="h1">THE STATE BUDGET AND ECONOMY CONTRACT THEMES</div>

<p>
<b>43-�-167</b> �Development of the design systems of optronic networks elements�. State register number # <b><u>0197U012587</u></b>
</p><p>
<b>43-�-198</b> Development of mathematical models of optronic UHF elements, based on gas transistors and facilities of digital processing of high-frequency signals�. State register number #  <b><u>0199U003434</u></b>
</p><p>
<b>43-�-261</b> �Development of mathematical models of microelectronic frequency transformers, based on the reactive properties of semiconductor devices with subzero resistance�. State register number #  <b><u>0102U002267</u></b>
</p><p>
<b>43-�-273</b> �Development of mathematical models of microelectronic radiomeasurement transformers for the systems of radial paraphase technology�. State register number # <b><u>0105U002419</u></b>
</p><p>
<b>4320</b> �Development of mathematical models and of principle charts of radiomeasurement transformers of gas charges�. State register number # <b><u>0108U009395</u></b>
</p><p>
<b>32-�-274</b> �Mathematical models of electric vibrations generators with the widerange re-erecting of generation frequency, based on the transistor structures with subzero resistance�. State register number <b><u>0105U002420</u></b> 
</p><p>
�Development of mathematical models of microelectronic frequency transformers of the magnetic field, based on the transistor structures with subzero resistance�. State register number  <b><u>0110U002160</u></b> (2010-2012 y.y.)
</p>


<?php
		break;
		case '4':
			$title = "Participating in international scientific and technical conferences and exhibitions of inventions";
?>

<br><br>
<!--<div class="h1">Participating in international scientific and technical conferences and exhibitions of inventions</div>-->

<img align="left" src="img_content/scientific_research/9.jpg" style="margin-left: 203px; margin-bottom: 0;">
<img src="img_content/scientific_research/10.jpg" style="margin-right: auto;">

<center style="font-size: 11px; width: 400px; margin-left: 158px; margin-top: 2px;">
	Doctor of science, prof. Osadchuk V.S. participating international exhibition of inventions in Budapest (Hungary) <br>
	�Genius-96�. (1996). Received reward .<br>
	for his invention: �Microelectronics gas sensor�.
</center>
<br><br>


<img align="left" src="img_content/scientific_research/11.jpg">
<p>�.�.�., Doctor of sciences, prof. Osadchuk V.S. and doctor of sciences, prof. Osadchuk O.V. took part in international scientific conference �Microcoll-99� in Budapest (Hungary) with the report: �RESEARCH OF THE SUPERHIGH FREQUENCY OSCILLATOR WITH OPTICAL CONTROL� in 1998. </p>
<div class="clr"></div>
<br><br>

<img align="left" src="img_content/scientific_research/12.jpg" style="margin-left: 163px">
<img src="img_content/scientific_research/13.jpg" style="margin-right: auto;">
<div class="clr"></div>


<?php
		break;
	}
?>