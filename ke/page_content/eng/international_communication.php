<?php

	$title = "International connections";
	
?>



<p>The department of electronics for the period from its foundation is legally considered to be one of the most innovative and progressive departments in our university. Most of which continue to enhance the prestige of the speciality. 5 doctors of sciences and more than 22 Ph.d have graduated the department. The graduates of the department usually occupy executive position in the organization. Such success is a result of laborious work of students and their proffesors and the carefully designed methods of teaching and mastering of material.</p>

<p>The training is managed in the well known all over the world scientific school under control of the doctor of science, professor, ac academician of the Ukrainian engineer academy, honoured science and technics worker, Osadchuk Volodymyr Stepanovych, and professor Y.Y. Bilinsiy, students begin to participate actively from the 2nd courses.</p>

<p>Every future graduating student of our department before his graduating the university institute is the fully formed specialist in electronics. Deep mastering of necessary knowledge is in specialities: «Microelectronics and semiconductor devices» and «Electronic devices», enables every our graduating student to work on the enterprise that needs the proper specialist. The department of Electronics conducts active international activity in obedience to basic work assignments on international cooperation which the following behaves to: 
<ul>
	<li>the assistance to scientific and technical collaboration with abroad organizations;</li> 
	<li>implementation of the general scientific programs, grants; providing information about competitions, grants, programs etc; help, in preparation of requests on participation in them;</li>
</ul>
</p>

<div style="text-align: center;">
	<img src="img_content/international_communication/1.jpg">
</div>



<p>The department carries out international scientific and technical cooperation through agreements in scientific and technical collaboration, participation, in international exhibitions, conferences, symposiums, participating in the international scientific programs, grants, through the research workers exchange. The department of Electronics carries out a collaboration in the sphere of educational and scientific activity: 
<ul>
	<li>Institute of superconductors physics, Poland;</li>
	<li>Kaunas technological university, Kaunas, Lithuania;</li>
	<li>Kaluga branch of Moscow state technical university of A.D. Bauman, Kaluga, Russia;</li>
	<li>Institute of superconductors physics, Donetsk , NAS Ukraine.</li>
</ul>
</p>

<p>The department has partner relationships with a few research establishments and scientisfic and manufacturing companies in Canada in a scientific and technical collaboration, in particular on programming of touch-controls parameters.</p>

