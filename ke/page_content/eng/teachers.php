<?php
	$title = "Professors and assistants";
?>

<img src="img_content/teachers/1.jpg" width="150" align="left">
<p><strong> Bilynskyy Yosyp Yosypovych </strong> doctor of science, professor of department of Electronics. Chief of the department. In 1983 years graduated Vinnytsya polytechnic institute with speciality �Automatics and telemechanics�. Worked in the research sector of department of automation and data measuring technics. Studied in post graduated courses. From 1991 works as a lecturer of department of computer automatisation of Vinnytsya polytechnic institute.</p> 
<p>In 1996 year defended Ph.d dissertation, in 2001 got the rank of associate professor, in 2009 year defended doctor dissertation.</p>
<p>Dissertation on a theme: �Subpixel methods of discrete measuring information processing in the computer optical-electronic systems�.</p>
<p>Published over 140 scientific publications: 2 monographs, 52 articles, 45 copyright certificates and patents, 4 workbooks. 12 master dissertations, 1 ph.d dissertation, and also 4 ph.d dissertations are executed under his control.</p>

<strong>Disciplines</strong> 
<ul>
	<li>�Digital circuit engineering�</li>
	<li>�Electronic systems� </li>
	<li>�Electronic calculating devices and systems�</li> 
	<li>�Interfaces of computer systems� </li>
	<li>�Basis of scientific research� </li>
	<li>�Developing electronic calculating devices and systems�</li>
	<li>�Mathematical methods of experiment planning�</li>
</ul>

<p><strong>�Basic research direction </strong> � optical-electronic measuring converters, digital methods of information processing and data measuring systems.</p>
<p>Subpixel methods of border detection and contour detection on the image were developed, optical-electronics devices: refractometr, range finder, microdeviation measurement system, foot diagnostics system, liquid capillary tension measurement system were designed and introduced under his control.</p> 
<p><strong>Lesure time: </strong> skiing, voleyball, windserfing.</p>


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/2.jpg" width="150" align="left">
<p><strong>Osadchuk Volodymyr Stepanovich </strong> doctor of science, professor of department of electronics, honoured scientist of Ukraine. Works in the university since the 1st January 1966. Published more than 500 scientific publications: 200 copyright certificates and patents, 7 monographs, 9 workbooks. Trained 22 ph.ds and 5 doctors of sciences.</p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�Microelectronic devices�</li> 
	<li>�Semiconductor and microelectronic devices�</li> 
	<li>�Electronic sensors�</li> 
	<li>�Microelectronic frequency sensors�</li> 
	<li>�Electronic sensors as primary data converters �</li> 
	<li>Integrate discipline</li> 
</ul>

<p class="no_indent"><strong>Scientific research :</strong> Research of reactive properties of semiconductor devices and microelectronic frequency transformers development, based on semiconductor structures with subzero resistance. Prof. Osadchuk V.S. manages large research work on the study of the physical phenomena in semiconductor and gas environments, development of microelectronic devices in the wide range of frequencies in the department.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/3.jpg" width="150" align="left">
<p><strong>Kravchenko Yuriy Stepanovych</strong> Ph.d, associated professor of department of electronic. Chief assistant in methodological work, scientific secretary of the department. Works in university since 1975. Published more than 60 scientifec publications </p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�Vacuum and plasma electronics�</li> 
	<li>�Solid electronics� </li>
	<li>�Functional electronics� </li>
	<li>�plasma-chemical processes in integrate chips technology�</li> 
	<li>�Semiconductor information converters� </li>
	<li>Master degree disciplines</li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong> research of plasma-chemical properties of unbalanced plasma and developing of electronic devices for the diagnostics and control of plasma-chemical processes.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/4.jpg" width="150" align="left">
<p><strong>Prokopov Igor Dmytrovych</strong> ph.d, associated professor of department of electronics. Works in the university since 1976. Published more than 70 scientific publications.</p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�Basis of electronic developing systems� </li>
	<li>�Microprocessors in automobiles� </li>
	<li>�Automobile primary data converters�</li> 
	<li>�Radio-electronic energysaving systems�</li> 
	<li>�CAD in energysaving electronics� </li>
	<li>�Modelling and optimization in electronic and optical-electronic systems�</li> 
	<li>�Modern branches of electronics� </li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong> of digital filters based on CPLD and FPGA.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/5.jpg" width="150" align="left">
<p><strong>Krylyk Ludmyla Viktorivna</strong> ph.d, associated professor of the department of electronics. Works in the university since 1987. Published more than 35 scientific publications.</p> 

<div class="clr"></div>
<strong>Discilpines:</strong> 
<ul>
	<li>�Personal computers�</li> 
	<li>�Programming and algorithmic languages�</li> 
	<li>�Calculating mathematics� </li>
	<li>�Data displaying devices�</li> 
	<li>�Materials of electronics� </li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong> : development of microelectronic frequency sensors of moisture based on negative capacitance structures.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/18.jpg" width="150" align="left"
<p><strong>Ogorodnyk Kostyantyn Volodymyrovych </strong> associated professor of the department of electronics. Published more 50 scientific publications, including monographs, manuals, 5 patents and certificates of copyright on the software.</p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�Simulation in electronics�</li> 
	<li>�The Energy Electronics�</li> 
	<li>�Laser equipment and technology� </li>
	<li>�Optoelectronics�</li> 
</ul>

<hr width="100%" size="1" noshade>


<img src="img_content/teachers/6.jpg" width="150" align="left">
<p><strong>Il�chenko Olena Mykolaivna </strong> post graduate student of the department of electronic. Studied at the master cource from 2007 to 2008. From 2008 is a full-time post graduate student in speciality "Radiomeasuring devices ". Published more than 15 publications, wined �� stage of Ukrainian competition of students research works in 2008.</p> 

<div class="clr"></div>

<p class="no_indent"><strong>Scientific research:</strong> development and research of microelectronic photosensitive sensors with frequency output based on the semiconductor structures with negative resistance and usage of such devices in automobile industry.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/8.jpg" width="150" align="left">
<p><strong>Prokopova Maria Oleksandrivna</strong> ph.d, associated professor of the department of electronics. Methodist of main character building center. Published more than 20 scientific publications.</p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�DBMS� </li> 
	<li>�Microprocessors� </li> 
	<li>�Electronic systems� </li> 
	<li>�Computer networks� </li> 
	<li>�Technological basis of electronics� </li> 
</ul>

<p class="no_indent"><strong>Scientific research:</strong> development of microelectronic frequency gas sensors based on structures with negative resistance</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/9.jpg" width="150" align="left">
<p><strong>Martynuk Volodymyr Valeriyovych </strong> ph.d., assistant professor of the department of electronics. Works in the university since 2001. Defended his ph.d dissertation in 2007. Published more than 15 scientific publications.</p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�Quantum mechanics � </li>
	<li>�Technology of electronic devices� </li>
	<li>�Statistical physics� </li>
	<li>�Theory of electromagnetic field� </li>
	<li>�Physics of solid body � </li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong> development of microelectronic frequency magnetic field sensors based on structures with negative resistance</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/13.jpg" width="150" align="left">
<p><strong>��������� ������ �����������</strong> ������� ������� �����������. ������� � ������� ����������� � ������� 2006 ���� �� ������� � ������ 2007 ����. � 1-�� ��������� 2007 ���� ����������� �� ���������� �� ����������� ��������. ���������, �� ����. </p> 

<div class="clr"></div>

<p class="no_indent"><strong>�������� ��������:</strong> ���������� ��������� ������� �������� ������������� �� ����� ��������� ��������������.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/10.jpg" width="150" align="left">

<p><strong>Seletska Olena Oleksandrivna </strong>associated professor of the department of electronics. Studied  at the master cource from 2005 to 2006. From 2006 is a correspondence post graduate student</p> 

<div class="clr"></div>
<strong>�Disciplines:</strong> 
<ul>
	<li>�Technology of semiconductor devices manufacturing and integrative circuits�</li>
	<li>�Control of semiconductor materials and microelectronic devices�</li>
	<li>�Functional electronics�</li>
	<li>�Practical training�</li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong> development of microelectronic frequency optical sensors based on structures with negative resistance for the plasm-chemical processes control.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/11.jpg" width="150" align="left">
<p><strong>Mel�nychuk Andriy Oleksandrovych </strong> post graduate student of the department of electronic. Studied at the master cource from 2007 to 2008. From 2008 is a full-time post graduate student in speciality "Devices for control and substance state defining". Published more than 10 publications.</p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�digital circuits engineering� </li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong> digital images processing in medical apparatus.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/14.jpg" width="150" align="left">
<p><strong>�Ratushnyy Pavlo Mykolayovych </strong> associated professor of the department of electronics. Studied at the master cource from 2005 to 2006. From 2006 is a full-time post graduate student in speciality "Devices for control and substance state defining". Published more than 10 publications.</p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>Electronic calculating devices and systems</li>
	<li>Interfaces of computer systems</li>
	<li>Basis of scientific research</li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong> objects defining in digital.</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/12.jpg" width="150" align="left">
<p><strong>Starovoitov Myhailo Antonovych </strong> Engineer of the 1st rank of the department of electronics, assistant professor. In 1963 � 1968 studied in Kyiv politechnical institute in speciality �Electronic devices�. Since 1968 works in VNTU. Published more than 15 scientific publications./p> 

<div class="clr"></div>
<strong>Disciplines:</strong> 
<ul>
	<li>�Vacuum technics�</li>
	<li>�electron-beam devices�</li>
</ul>

<p class="no_indent"><strong>Scientific research:</strong>gas-unloading electronics, high-voltage smouldering digit, gas-unloading cannons, systems of the gas-dynamic adjusting of cannons parameters</p> 


<hr width="100%" size="1" noshade>


<img src="img_content/teachers/18.jpg" width="150" align="left">
<p><strong>Kravchenko Sergiy Yuriyovych </strong> post graduate student of the department of electronic. Studied at the master cource from 2007 to 2008. From 2008 is a full-time post graduate student. Published more than 10 publications. </p> 

<div class="clr"></div>

<p class="no_indent"><strong>Scientific research:</strong> microelectronic optical transformers on the basis of structures with subzero resistance for plasma-chemical processes.</p> 

<hr width="100%" size="1" noshade>

<div style="font-weight: bold; text-align: center; margin: 10px 0;">Personnel Department</div>
<div style="width: 700px;">
	<div style="float:left; margin-right: 40px; margin-left: 25px">
		<img src="img_content/teachers/15.jpg" width="150">
		<p style="text-indent: 0px;"><strong>Smetana A.V. </strong><br> chief of laboratory.</p> 
	</div>
	<div style="float:left; margin-right: 40px;">
		<img src="img_content/teachers/16.jpg" width="150">
		<p style="text-indent: 0px;"><strong>Gladko Y.K. </strong><br> engineer .</p> 
	</div>
	<div>
		<img src="img_content/teachers/17.jpg" width="150">
		<p style="text-indent: 0px;"><strong>Reshetnik A.P.</strong><br> secretary.</p> 
	</div>
</div>
