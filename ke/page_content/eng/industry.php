<?php
	$title = "FIELD OF ELECTRONICS";
?>

<div class="h1">Electronics</div>

<p><strong> Electronics</strong> � is one of the most extraordinary branches of science and technics with the possibilities, it�s hard to imagine life of modern society without it. It allowed to connect the remote cites of earth, get to the depths of space and bowels of atom�s core, automatize the most difficult production processes. Electronics is widely used in the technics of physical experiment,  in medical practice, in an auto and railway transport, in life.</p>

<img src="img_content/industry/1.jpg" align="left">


<p>Progress is unthinkable in none of civilization industries without microelectronics and computing engineering, one of perspective scientific and technical directions. The rates of microelectronics development, highly productive computers, information processing systems, breadth and efficiency of their introduction, in all industries of human activity do not have analogues in history of science and technique.</p>

<p><strong>Electronics</strong> � is a branch of science and technique that includes study and application of the electronic and ionic phenomena in different environments and on their boundaries, thus a basic place in electronics is occupied by electronic devices. Their number in complicated electronic devices can reach to hundreds of thousands.</p>

<p>Electronic devices are devices, in which conductivity is carried out mainly by electrons or ions, that move in a vacuum, eider or semiconductor. Electronic devices are devices, in which physical phenomena, related to motion of the charged particles in a solid, vacuum, or in a gas environment, are used.</p>
<br>
<center>
<div style="width: 500px; background-color: #DEDFDF;">
	<img src="img_content/industry/2.jpg">
	<div style="width: 125; float: left; font-size: 10px; text-align: center;">
		Hybrid integrated circuit of frequency magnetic touch-control developed in the department
	</div>
	<div style="width: 125; float: left; font-size: 10px; text-align: center;">
		Diaphragm touch-control of pressure
	</div>
	<div style="width: 125; float: left; font-size: 10px; text-align: center;">
		Integrated circuit of frequency touch-control of temperature
	</div>
	<div style="width: 125; float: left; font-size: 10px; text-align: center;">
		Photosensetivce sensor
	</div>
	
</div>
</center>
<br><br><br><br><br>

<img src="img_content/industry/3.jpg" align="left">

<p>Successes in electronics development, and especially microelectronics, are laid in modern scientific and technical revolution. The swiftness of technique and technology of the integrated circuits development is stipulated by this factor and the rapid growth of basic parameter that characterizes technical level of the integrated circuits - integration degree as well.</p>
<div class="clr"></div>



<div class="h1">Landmarks of development of microelectronics</div>


<p>The end of 40th is the invention of BPT, introduction, it to the industrial production of discrete BPT.</p>
<p>The end of 50th - beginning of 60th is the invention of transistor based on metal-oxide-semiconductor structure; development of the first integral microcircuits (IMS) with the integration degree up to few units - ten components in a silicon crystal.</p> 
<p>End of 60th � about a hundred of active components bipolar or MOS - transistors are placed in the in the integrated circuits of middle integration degree.</p>
<p>Middle of 70th - exceeded the integration degree of large-scale integrated circuits of more than 103 components.</p>
<p>Beginning of 80th � the integration degree in the ultralarge integrated circuits  grew to 105 active components.</p> 
<p>A middle of 80th - the integration degree in ultralarge integrated circuits exceeded 106-107 active components.</p>
<p>Prognosis to 2010 year - microelectronic highly productive facilities of information processing will be organically involved in all industries of human activity, the new type of infrastructure will appear in development of civilization - informative, sharply the labour productivity will be increased in scientific activity and considerable socially economic progress will be well-to-do.</p> 


<br>
<center>
<div style="width: 500px; background-color: #DEDFDF;">
	<img src="img_content/industry/4.jpg">
	<div style="width: 250; float: left; font-size: 10px; text-align: center;">
		Materials, used in microelectronics  
	</div>
	<div style="width: 125; float: left; font-size: 10px; text-align: center;">
		Light-emitting diodes and semiconductor lasers
	</div>
	<div style="width: 125; float: left; font-size: 10px; text-align: center;">
		Miniature microelectronic microscope 
	</div>
	
</div>
</center>

